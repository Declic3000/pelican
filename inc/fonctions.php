<?php

function getValeur($tab, $id = null, $champs = null, $substitution = '')
{
    if ($id !== null) {
        if (isset($tab[$id])) {
            if ($champs) {
                return $tab[$id][$champs] ?? $substitution;
            } else {
                return $tab[$id];
            }
        } else {
            return $substitution;
        }
    }
    return $tab;
}


/**
 * dessinerUneBranche
 * @param $arbre
 * @param $noeud
 * @param $branche
 * @return mixed
 */
function dessinerUneBranche($arbre, $noeud, $branche)
{
    $path_array = explode('.', (string)$noeud);
    $temp = &$arbre;
    foreach ($path_array as $key) {
        if (!isset($temp[$key])) {
            $temp[$key] = [];
        }
        $temp = &$temp[$key];
    }
    $temp = $branche;
    return $arbre;
}


function dessinerUneBrancheGreffe($arbre, $noeud, $branche, $greffe = '')
{
    $path_array = explode('.', (string)$noeud);
    $temp = &$arbre;
    foreach ($path_array as $key) {

        if (!isset($temp[$greffe][$key])) {
            $temp[$greffe][$key] = [];
        }
        $temp = &$temp[$greffe][$key];
    }
    $temp = $branche;
    return $arbre;
}


function rand_sha1($length)
{
    $max = ceil($length / 40);
    $random = '';
    for ($i = 0; $i < $max; $i++) {
        $random .= sha1(microtime(true) . mt_rand(10000, 90000));
    }
    return substr($random, 0, $length);
}


function donneDateSemaineNumero(int $annee, int $num_semaine, int $nb_jour_suppl = 0)
{

    [$date, $date_debut, $date_fin] = donneDateSemaine($annee . '-01-01');
    if ($date->format('W') > 1) {
        [$date, $date_debut, $date_fin] = donneDateSemaine($annee . '-01-05');
    }
    $nb = (($num_semaine - 1) * 7 + $nb_jour_suppl);
    $date_debut->add(new DateInterval('P' . $nb . 'D'));
    return $date_debut;

}


function donneDateSemaine($date_param = null)
{
    if ($date_param === null) {
        $date_param = (new DateTime())->format('Y-m-d');
    }
    $date = DateTime::createFromFormat('Y-m-d', $date_param);
    $jour_de_la_semaine = $date->format('N');
    $date_semaine = (clone($date));


    $lundi = (clone($date_semaine))->sub(new DateInterval('P' . ($jour_de_la_semaine - 1) . 'D'));
    $dimanche = (clone($lundi))->add(new DateInterval('P6D'));
    return [$date, $lundi, $dimanche];

}


