<?php


function natksort($a, $b): int
{
    $a = strtr($a, "ÄÖÜäöüÉÈÀËëéèàç", "AOUaouEEAEeeeac");
    $b = strtr($b, "ÄÖÜäöüÉÈÀËëéèàç", "AOUaouEEAEeeeac");
    return strcasecmp($a, $b);
}

/**
 * @throws Exception
 */
function objectToArray($d)
{
    if (is_object($d)) {
        if (isset($d->date) && isset($d->timezone)) {
            $d = new DateTime($d->date, new DateTimeZone($d->timezone));
        } else {
            $d = get_object_vars($d);
        }
    }

    if (is_array($d)) {
        return array_map(__FUNCTION__, $d);
    } else {
        return $d;
    }
}

/**
 * @throws Exception
 */
function detecte_datetime($array)
{
    if (is_array($array)) {
        if (isset($array['date']) && isset($array['timezone'])) {
            $array = date_create($array['date'], new DateTimeZone($array['timezone']));
        } else {
            foreach ($array as &$a) {
                $a = detecte_datetime($a);
            }
        }
    }
    return $array;
}

function camelizeKeys($array): array
{
    $tab = [];
    foreach ($array as $k => $a) {
        $tab[camelize2($k)] = $a;
    }
    return $tab;
}

function camelize2($chaine): string
{
    $words = explode('_', (string)$chaine);
    $words = array_map('ucfirst', $words);
    return lcfirst(implode('', $words));
}

function camelize($chaine)
{
    return str_replace('_', '', ucwords((string)$chaine, '_'));
}

function decamelize($input): string
{
    $matches = [];
    preg_match_all('!([A-Z][A-Z0-9]*(?=$|[A-Z][a-z0-9])|[A-Za-z][a-z0-9]+)!', (string)$input, $matches);
    $ret = $matches[0];
    foreach ($ret as &$match) {
        $match = $match == strtoupper($match) ? strtolower($match) : lcfirst($match);
    }
    return implode('_', $ret);
}

function sanitize_file_name($string, $force_lowercase = true, $anal = false)
{
    $strip = ["~", "`", "!", "@", "#", "$", "%", "^", "&", "*", "(", ")", "_", "=", "+", "[", "{", "]", "}", "\\", "|", ";", ":", "\"", "'", "&#8216;", "&#8217;", "&#8220;", "&#8221;", "&#8211;", "&#8212;", "â€”", "â€“", ",", "<", ".", ">", "/", "?"];
    $clean = trim(str_replace($strip, "", strip_tags((string)$string)));
    $clean = preg_replace('/\s+/', "-", $clean);
    $clean = ($anal) ? preg_replace("/[^a-zA-Z0-9]/", "", (string)$clean) : $clean;
    return ($force_lowercase) ?
        (function_exists('mb_strtolower')) ?
            mb_strtolower((string)$clean, 'UTF-8') :
            strtolower((string)$clean) :
        $clean;
}

function normaliser($str)
{
    $charMap = [
        'a' => '/[àáâã]/iu',
        'c' => '/ç/iu',
        'e' => '/[èéêë]/iu',
        'i' => '/[ïíî]/iu',
        'o' => '/[ôó]/iu',
        'oe' => '/œ/iu',
        'u' => '/[üúû]/iu'
    ];
    $keys = array_keys($charMap);
    $values = array_values($charMap);
    return preg_replace($values, $keys, (string)$str);
}


function tableauChemin($tab, $chemin)
{

    if (!is_array($chemin)) {
        $chemin = explode('.', (string)$chemin);
    }
    foreach ($chemin as $key) {
        if (isset($tab[$key])) {
            $tab = $tab[$key];
        } else {
            return null;
        }
    }
    return $tab;
}


function majuscule($texte, $mode = 1)
{
    if ($mode === 1) {
        if (function_exists('mb_strtoupper')) {
            $texte = mb_strtoupper((string)$texte);
        } else {
            $texte = strtoupper((string)$texte);
        }
    } elseif ($mode === 2) {
        $texte = ucwords((string)$texte);
    }
    return $texte;
}


function minuscule($texte)
{

    if (function_exists('mb_strtolower')) {
        $texte = mb_strtolower((string)$texte);
    } else {
        $texte = strtolower((string)$texte);
    }
    return $texte;
}


function chiffre_en_lettre($montant, $devise1 = '', $devise2 = ''): string
{
    $unite = [];
    $dix = [];
    $cent = [];
    $prim = [];
    $secon = [];
    $trio = [];
    if (empty($devise1)) {
        $dev1 = 'euros';
    } else {
        $dev1 = $devise1;
    }
    if (empty($devise2)) {
        $dev2 = 'centimes';
    } else {
        $dev2 = $devise2;
    }
    $valeur_entiere = intval($montant);
    $valeur_decimal = intval(round($montant - intval($montant), 2) * 100);
    $dix_c = intval($valeur_decimal % 100 / 10);
    $cent_c = intval($valeur_decimal % 1000 / 100);
    $unite[1] = $valeur_entiere % 10;
    $dix[1] = intval($valeur_entiere % 100 / 10);
    $cent[1] = intval($valeur_entiere % 1000 / 100);
    $unite[2] = intval($valeur_entiere % 10000 / 1000);
    $dix[2] = intval($valeur_entiere % 100000 / 10000);
    $cent[2] = intval($valeur_entiere % 1000000 / 100000);
    $unite[3] = intval($valeur_entiere % 10000000 / 1000000);
    $dix[3] = intval($valeur_entiere % 100000000 / 10000000);
    $cent[3] = intval($valeur_entiere % 1000000000 / 100000000);
    $chif = ['', 'un', 'deux', 'trois', 'quatre', 'cinq', 'six', 'sept', 'huit', 'neuf', 'dix', 'onze', 'douze', 'treize', 'quatorze', 'quinze', 'seize', 'dix sept', 'dix huit', 'dix neuf'];
    $trio_c = '';
    for ($i = 1; $i <= 3; $i++) {
        $prim[$i] = '';
        $secon[$i] = '';
        $trio[$i] = '';
        if ($dix[$i] == 0) {
            $secon[$i] = '';
            $prim[$i] = $chif[$unite[$i]];
        } else {
            if ($dix[$i] == 1) {
                $secon[$i] = '';
                $prim[$i] = $chif[($unite[$i] + 10)];
            } else {
                if ($dix[$i] == 2) {
                    if ($unite[$i] == 1) {
                        $secon[$i] = 'vingt et';
                    } else {
                        $secon[$i] = 'vingt';
                    }
                    $prim[$i] = $chif[$unite[$i]];
                } else {
                    if ($dix[$i] == 3) {
                        if ($unite[$i] == 1) {
                            $secon[$i] = 'trente et';
                        } else {
                            $secon[$i] = 'trente';
                        }
                        $prim[$i] = $chif[$unite[$i]];
                    } else {
                        if ($dix[$i] == 4) {
                            if ($unite[$i] == 1) {
                                $secon[$i] = 'quarante et';
                            } else {
                                $secon[$i] = 'quarante';
                            }
                            $prim[$i] = $chif[$unite[$i]];
                        } else {
                            if ($dix[$i] == 5) {
                                if ($unite[$i] == 1) {
                                    $secon[$i] = 'cinquante et';
                                } else {
                                    $secon[$i] = 'cinquante';
                                }
                                $prim[$i] = $chif[$unite[$i]];
                            } else {
                                if ($dix[$i] == 6) {
                                    if ($unite[$i] == 1) {
                                        $secon[$i] = 'soixante et';
                                    } else {
                                        $secon[$i] = 'soixante';
                                    }
                                    $prim[$i] = $chif[$unite[$i]];
                                } else {
                                    if ($dix[$i] == 7) {
                                        if ($unite[$i] == 1) {
                                            $secon[$i] = 'soixante et';
                                        } else {
                                            $secon[$i] = 'soixante';
                                        }
                                        $prim[$i] = $chif[$unite[$i] + 10];
                                    } else {
                                        if ($dix[$i] == 8) {
                                            if ($unite[$i] == 1) {
                                                $secon[$i] = 'quatre-vingts et';
                                            } else {
                                                $secon[$i] = 'quatre-vingt';
                                            }
                                            $prim[$i] = $chif[$unite[$i]];
                                        } else {
                                            if ($dix[$i] == 9) {
                                                if ($unite[$i] == 1) {
                                                    $secon[$i] = 'quatre-vingts et';
                                                } else {
                                                    $secon[$i] = 'quatre-vingts';
                                                }
                                                $prim[$i] = $chif[$unite[$i] + 10];
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        if ($cent[$i] == 1) {
            $trio[$i] = 'cent';
        } else {
            if ($cent[$i] != 0 || $cent[$i] != 0) {
                $trio[$i] = $chif[$cent[$i]] . ' cents';
            }
        }
    }

    $chaine = '';
    $chif2 = ['', 'dix', 'vingt', 'trente', 'quarante', 'cinquante', 'soixante', 'soixante-dix', 'quatre-vingts', 'quatre-vingts dix'];
    $secon_c = $chif2[$dix_c];
    if ($cent_c == 1) {
        $trio_c = 'cent';
    } else {
        if ($cent_c != 0 || $cent_c != 0) {
            $trio_c = $chif[$cent_c] . ' cents';
        }
    }

    if (($cent[3] == 0 || $cent[3] == 0) && ($dix[3] == 0 || $dix[3] == 0) && ($unite[3] == 1)) {
        $chaine .= $trio[3] . '  ' . $secon[3] . ' ' . $prim[3] . ' million ';
    } else {
        if (($cent[3] != 0 && $cent[3] != 0) || ($dix[3] != 0 && $dix[3] != 0) || ($unite[3] != 0 && $unite[3] != 0)) {
            $chaine .= $trio[3] . ' ' . $secon[3] . ' ' . $prim[3] . ' millions ';
        } else {
            $chaine .= $trio[3] . ' ' . $secon[3] . ' ' . $prim[3];
        }
    }

    if (($cent[2] == 0 || $cent[2] == 0) && ($dix[2] == 0 || $dix[2] == 0) && ($unite[2] == 1)) {
        $chaine .= ' mille ';
    } else {
        if (($cent[2] != 0 && $cent[2] != 0) || ($dix[2] != 0 && $dix[2] != 0) || ($unite[2] != 0 && $unite[2] != 0)) {
            $chaine .= $trio[2] . ' ' . $secon[2] . ' ' . $prim[2] . ' milles ';
        } else {
            $chaine .= $trio[2] . ' ' . $secon[2] . ' ' . $prim[2];
        }
    }

    $chaine .= $trio[1] . ' ' . $secon[1] . ' ' . $prim[1];

    $chaine .= ' ' . $dev1 . ' ';

    if (($cent_c == '0' || $cent_c == 0) && ($dix_c == '0' || $dix_c == 0)) {
        $chaine .= ' et z&eacute;ro ' . substr((string)$dev2, 0, -1);
    } else {
        $chaine .= $trio_c . ' ' . $secon_c . ' ' . $dev2;
    }
    return $chaine;
}


function afficher_telephone($numero, string $sep = ' '): string
{
    $numero = preg_replace('/[^0-9]/', '', (string)$numero);

    if (strlen((string)$numero) != 10) {
        return $numero;
    } else {
        return chunk_split((string)$numero, 2, $sep);
    }
}


function arbre($temp, $direct = true): string
{
    $chaine = '';
    if (is_array($temp)) {
        $chaine .= '<ul>';
        foreach ($temp as $key => $niveau) {
            $chaine .= '<li><strong>' . $key . '</strong>';
            if (is_array($niveau)) {
                $chaine .= arbre($temp[$key], false);
            } else {
                $chaine .= ' - <span class="type_var">' . gettype($niveau) . " : </span>";
                if (is_object($niveau)) {
                    $chaine .= '- est un objet' . '<BR><span class="type_var">objet</span>';
                    $chaine .= var_export($niveau, true);
                } else {
                    $chaine .= $niveau;
                }
            }
            $chaine .= '</li>';
        }
        $chaine .= '</ul>';
    } else {
        $chaine .= '<ul><li>' . $temp . '</li></ul>';
    }
    if ($direct) {
        print $chaine;
        return '';
    }
    return $chaine;
}


function chaineAleatoire($nb_car, $chaine = 'azertyuiopqsdfghjklmwxcvbn0123456789'): string
{
    $nb_lettres = strlen((string)$chaine) - 1;
    $generation = '';
    for ($i = 0; $i < $nb_car; $i++) {
        $pos = mt_rand(0, $nb_lettres);
        $car = $chaine[$pos];
        $generation .= $car;
    }
    return $generation;
}

function creer_uniqid(): string
{
    static $seeded;

    if (!$seeded) {
        $seed = (double)(microtime(true) + 1) * time();
        mt_srand($seed);
        mt_srand($seed);
        $seeded = true;
    }

    $s = mt_rand();
    if (!$s) {
        $s = random_int(0, mt_getrandmax());
    }
    return uniqid($s, 1);
}


function array2csv($array, $separateur = "\t"): string
{
    $tab = [];
    foreach ($array as $row) {
        $tab[] = implode($separateur, $row);
    }
    return implode(PHP_EOL, $tab);
}


function csv2array($chaine, $separateur = "\t")
{
    $tab = explode(PHP_EOL, (string)$chaine);
    foreach ($tab as &$row) {
        $row = explode($separateur, $row);
    }
    return $tab;
}


function array2htmltable($array, $options = []): string
{
    $table = '';
    if (count($array) >= 2) {//minimum 2 lignes 1 titre 1 data
        $class_table = ' ' . ($options['th'] ?? '');
        $table_entete = array_shift($array);
        $cols = [];
        foreach ($array as $row) {
            foreach ($row as $key2 => $row2) {
                $cols[] = $key2;
            }
            $cols = array_unique($cols);
        }

        $table = '<table class="table table-striped' . $class_table . '">';
        $table .= '<thead><tr>';
        $i = 0;
        foreach ($table_entete as $row2) {
            $style = "";
            if (isset($options['largeur'][$i])) {
                $style = ' style="width : ' . $options['largeur'][$i] . ';"';
            }
            $table .= '<th' . $style . ' align="left">' . $row2 . '</th>';
            $i++;
        }
        $table .= '</tr></thead>';


        foreach ($array as $key => $row) {
            $table .= '<tr>';
            $style = "";
            if (count($cols) >= 2) {
                $i = 0;
                foreach ($cols as $key2) {
                    $row2 = $row[$key2] ?? '';
                    if (isset($options['largeur'][$i])) {
                        $style = ' style="width : ' . $options['largeur'][$i] . ';"';
                    }
                    $table .= '<td class="td_style' . $key2 . '"' . $style . '>' . $row2 . '</td>';
                    $i++;
                }
            } else {//permettre 1 colonne
                if (isset($options['largeur'][$i])) {
                    $style = ' style="width : ' . $options['largeur'][$i] . ';"';
                }
                $table .= '<td class="td_style' . $key . '"' . $style . '>' . $row . '</td>';
                $i++;
            }

            $table .= '</tr>';
        }
        $table .= '</table>';
    }
    return $table;
}

