<?php

namespace Declic3000\Pelican\Tache;

use Declic3000\Pelican\Service\Facteur;
use Declic3000\Pelican\Service\LogMachine;
use Declic3000\Pelican\Service\Sac;
use Declic3000\Pelican\Service\Suc;
use Declic3000\Pelican\Service\Sucre;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Routing\Router;

class TacheSup extends Tache
{

    protected $facteur;
    protected $log;
    protected $suc;
    protected $router;

    function __construct(Sac $sac, Suc $suc, EntityManagerInterface $em, Facteur $facteur, LogMachine $log, Router $router)
    {
        parent::__construct($sac, $em);
        $this->facteur = $facteur;
        $this->log = $log;
        $this->suc = $suc;
        $this->router = $router;
    }


}