<?php

namespace Declic3000\Pelican\Tache;

use Declic3000\Pelican\Component\Form\FormFactory;
use Declic3000\Pelican\Service\Facteur;
use Declic3000\Pelican\Service\LogMachine;
use Declic3000\Pelican\Service\Requete;
use Declic3000\Pelican\Service\Sac;
use Declic3000\Pelican\Service\Suc;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Routing\RouterInterface;


class TacheSupSup extends TacheSup
{

    protected $formbuilder;
    protected $requete;

    function __construct(Sac $sac, Suc $suc, EntityManagerInterface $em, Facteur $facteur, LogMachine $log, RouterInterface $router, FormFactory $formbuilder, Requete $requete)
    {
        parent::__construct($sac, $suc, $em, $facteur, $log, $router);
        $this->formbuilder = $formbuilder;
        $this->requete = $requete;
    }


}