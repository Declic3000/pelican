<?php

namespace Declic3000\Pelican\Tache;

use Declic3000\Pelican\Service\Sac;
use Doctrine\ORM\EntityManagerInterface;


class Tache
{

    protected $sac;
    protected $em;
    protected $db;
    protected $avancement;
    protected $nb_phase = 1;
    protected $message_log = [];
    protected $finie = false;
    protected $interaction = false;
    protected $args;


    function __construct(Sac $sac, EntityManagerInterface $em)
    {
        $this->sac = $sac;
        $this->db = $em->getConnection();
        $this->em = $em;
    }

    function tache_init($avancement = [], $args = [])
    {
        $this->avancement = $avancement;
        $this->args = $args;
    }

    function getInteraction()
    {
        return $this->interaction;
    }

    function setInteraction(bool $interaction)
    {
        $this->interaction = $interaction;
    }

    /**
     * @return mixed
     */
    public function getAvancement()
    {
        return $this->avancement;
    }

    public function setAvancement(mixed $avancement): void
    {
        $this->avancement = $avancement;
    }

    /**
     * @return float
     */
    public function getProgression(): float
    {
        if ($this->nb_phase === 1) {
            return floor(($this->avancement['nb'] / $this->avancement['nb_initial']) * 100);
        } else {
            $part_phase = 1 / $this->nb_phase;
            $pourcent = (max(($this->avancement['phase'] - 1), 0) * $part_phase);
            if (isset($this->avancement['nb_initial']) && $this->avancement['nb_initial'] > 0) {
                $pourcent += (($this->avancement['nb'] / $this->avancement['nb_initial'])) * $part_phase;
            }
            $pourcent = floor($pourcent * 100);
            return $pourcent;
        }
    }

    /**
     * @return int
     */
    public function getNbPhase(): int
    {
        return $this->nb_phase;
    }

    /**
     * @param int $nb_phase
     */
    public function setNbPhase(int $nb_phase): void
    {
        $this->nb_phase = $nb_phase;
    }


    function est_finie()
    {
        return $this->finie;
    }

    function tache_run()
    {
        return $this->finie;
    }

    /**
     * @return array
     */
    public function getMessageLog()
    {
        return $this->message_log;
    }

    /**
     * @param array $message_log
     */
    public function setMessageLog(array $message_log): void
    {
        $this->message_log = $message_log;
    }

    /**
     * @return void
     */
    public function addMessageLog(string $message)
    {
        $this->message_log[] = $message;
    }

    /**
     * @return void
     */
    public function addMessagesLog(array $messages)
    {
        $this->message_log = $this->message_log + $messages;
    }


}