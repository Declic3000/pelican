<?php

namespace Declic3000\Pelican\Event;

use Symfony\Contracts\EventDispatcher\Event;

class MenuModifEvent extends Event
{
    public function __construct(private $returnValue)
    {
    }

    public function getMenu()
    {
        return $this->returnValue;
    }

    public function setMenu($returnValue)
    {
        $this->returnValue = $returnValue;
    }
}