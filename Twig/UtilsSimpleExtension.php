<?php

namespace Declic3000\Pelican\Twig;

use DateTime;
use Declic3000\Pelican\Component\Filtre\Filtre;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

class UtilsSimpleExtension extends AbstractExtension
{


    public function getFilters(): array
    {
        return [
            new TwigFilter('array_values', fn($tab) => array_values($tab)),

            new TwigFilter('liste_filtre', fn($tab_filtre) => Filtre::liste_filtre($tab_filtre)),

            new TwigFilter('closingTag', function ($opentag) {
                if ($pos = strpos($opentag, ' ')) {
                    $opentag = substr($opentag, 0, $pos) . '>';
                }
                $opentag = str_replace('<', '</', $opentag);
                return $opentag;
            }),

            new TwigFilter('classLog', fn($code_log) => class_css_objet(substr((string)$code_log, 4, 3), 'fa-check')),

            new TwigFilter('classLogColor', fn($code_log) => class_css_action(substr((string)$code_log, 7, 3))),

            new TwigFilter('exclure_cles', function ($tab_value, $tab_keys) {

                foreach ($tab_keys as $key) {
                    unset($tab_value[$key]);
                }
                return $tab_value;
            }),

            new TwigFilter('lien_courriel', function ($email) {
                if (!empty($email)) {
                    return '<a href="mailto:' . $email . '">' . $email . '</a>';
                }
                return '';
            }, ['is_safe' => ['html']]),
            new TwigFilter('ouinon', fn($bool, $inverser = false) => $bool !== $inverser ? 'oui' : 'non'),

            new TwigFilter('getAutorisationProfil', fn(string $type) => getProfil($type)),

            new TwigFilter('getAutorisationNiveau', fn(string $num) => getNiveau($num)),

            new TwigFilter('array2htmltable', fn($table, $options = []) => array2htmltable($table, $options), ['is_safe' => ['html']]),

            new TwigFilter('afficher_telephone', fn($num) => afficher_telephone($num)),

            new TwigFilter('afficher_iban', fn($iban) => implode(' ', str_split((string)$iban, 4))),
            new TwigFilter('sanitize', fn($texte) => sanitize_file_name($texte)),

            new TwigFilter('image', fn($url, $largeur = '200px') => '<img src="' . $url . '" alt="logo" align="left" class="img-thumbnail" width="' . $largeur . '" />', ['is_safe' => ['html']]),


        ];
    }

    public function getFunctions(): array
    {
        return [


            new TwigFunction('table_filtrer_valeur', fn($table, $cle, $value) => table_filtrer_valeur($table, $cle, $value)),
            new TwigFunction('table_trier_par', fn($table, $cle, $ordre = SORT_ASC) => table_trier_par($table, $cle, $ordre)),

            new TwigFunction('decode', function ($type, $code = null) {
                $type_autorise = [
                    'regle' => 'Regle',
                    'type_op' => 'TypeOp',
                    'etat' => 'Etat'
                ];
                if (isset($type_autorise[$type])) {
                    $nom_fonction = 'getListe' . $type_autorise[$type];
                    return $nom_fonction($code);
                }
                return $code;
            }),

            new TwigFunction('convertDate', fn(string $chaine, string $format) => DateTime::createFromFormat($format, $chaine)),

            new TwigFunction('transformeTD', function ($cellule, $largeur, $html = 'td') {
                $css = [];
                $css[] = 'style' . ($cellule['style'] ?? '0');
                $css[] = 'align' . (isset($cellule['alignement']) ? strtolower((string)$cellule['alignement']) : 'c');
                $css[] = 'bordure' . (isset($cellule['bordure']) ? strtolower((string)$cellule['bordure']) : '1');


                $chaine = '<' . $html . ' class="' . implode(' ', $css) . '" width="' . $largeur . 'px" >';
                $chaine .= $cellule['libelle'] ?? '';
                $chaine .= '</' . $html . '>';
                return $chaine;
            }),


            new TwigFunction('str_pad', fn($chaine, $pad_length = 2, $pad_string = ' ', $direction = STR_PAD_LEFT) => str_pad((string)$chaine, $pad_length, $pad_string, $direction))


        ];
    }
}



