<?php

namespace Declic3000\Pelican\Service;


use App\Form\LogoType;
use Exception;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;


class ControllerObjet extends Controller
{

    function index_defaut($objet = null, $options_table = [])
    {
        if (!$objet) {
            $objet = $this->sac->get('objet');
        }
        $descr_objet = $this->sac->descr($objet);

        if ($descr_objet['autorisation_lecture'] && !$this->gendarme->autoriser('OBJET_' . $descr_objet['groupe'] . '_VIS')) {

            return $this->redirect($this->generateUrl('erreur403'));
        }

        $mode_affichage = ($this->suc->pref($objet . '.index.datatable.type_affichage') == 2) ? 'datagrid' : 'datatable';
        $options_table = $options_table + ['mode_affichage' => $mode_affichage];
        $table = $this->createTable($objet, $options_table, $this->requete->get('type_table') ?? $objet);

        if ($this->requete->estAjax()) {
            $action = $this->requete->get('action');
            switch ($action) {
                case 'datagrid':
                case 'dataliste':
                    return $this->json($table->objet_dataliste());
                    break;
                case 'autocomplete':
                    return $this->json($table->liste_autocomplete());
                    break;
            }
        } else {
            $options_args = $options_table['args'] ?? [];
            $args_twig = [
                'objet_bloc' => $objet,
                'datatable' => $table->export_twig(true, $options_args)
            ];
            $args_twig += $options_table['args_twig'] ?? [];
            return $this->render($this->sac->fichier_twig('index', $objet), $args_twig);
        }
    }

    function export_defaut(Exporteur $exporteur, $objet = null, $options_table = [])
    {
        if (!$objet) {
            $objet = $this->sac->get('objet');
        }
        $descr_objet = $this->sac->descr($objet);
        if (($descr_objet['autorisation_lecture'] ?? false) && !$this->gendarme->autoriser('OBJET_' . $descr_objet['groupe'] . '_VIS')) {
            return $this->redirect($this->generateUrl('erreur403'));
        }
        $db = $this->em->getConnection();
        $selecteur = new Selecteur($this->requete, $db, $this->sac, $this->suc);
        $selecteur->setObjet($objet);

        $options_args = $options_table['args'] ?? [];
        $sql = $selecteur->getSelectionObjet(null, '*');
        $tab_data = $db->fetchAllAssociative($sql);
        $tab_entete = array_keys($tab_data[0]);
        array_unshift($tab_data, $tab_entete);
        return $exporteur->export_tableur($this->sac->get('objet') . "_export", $tab_data, 'xls');
    }

    function new_defaut($args = [], $objet = null, $options = [], $reponse = true, $args_rep = [], $nom_class_form = null)
    {
        if (!$objet) {
            $objet = $this->sac->get('objet');
        }
        $descr_objet = $this->sac->descr($objet);
        // Controle autorisation
        if (!$this->gendarme->autoriser('OBJET_' . $descr_objet['groupe'] . '_CRE')) {
            return $this->redirect($this->generateUrl('erreur403'));
        }
        return $this->form_default(false, null, $args, $objet, $options, $reponse, $args_rep, $nom_class_form);
    }

    private function form_default(bool $modification, $objet_data = null, $args = [], $objet = null, $options_form = [], $reponse = true, $args_rep = [], $object_classe_form = null)
    {
        if (!$objet) {
            $objet = $this->sac->get('objet');
        }
        $descr_objet = $this->sac->descr($objet);
        $object_classe_form = camelize($object_classe_form ?? $objet);

        if (!$modification) {


            $objet_alias = empty($descr_objet['alias']) ? $objet : $descr_objet['alias'];
            $nom_classe_entity = '\\App\\Entity\\' . camelize($objet_alias);
            $objet_data = new $nom_classe_entity();

        }
        $class_action = 'App\\Action\\' . $object_classe_form . 'Action';


        // Initialisation complémentaire des données
        if (method_exists($class_action, 'chargementData')) {
            $action = $this->classAction($class_action);
            $data = $action->chargementData($modification, $objet_data);
        } else {
            $data = $objet_data;
        }

        // Construction du formulaire
        $nom_classe_form = 'App\\Form\\' . $object_classe_form . 'Type';
        $formbuilder = $this->pregenerateForm($objet . 'form', $nom_classe_form, $data, $options_form, $modification, $args);

        // Compléter la construction du formulaire
        if (method_exists($class_action, 'generareFormCplt')) {
            $action = $this->classAction($class_action);
            $action->generareFormCplt($modification, $data, $formbuilder);
        } else {
            $formbuilder->add('submit', SubmitType::class, ['label' => $modification ? 'Modifier' : 'Ajouter', 'attr' => ['class' => 'btn-primary']]);
        }

        if (method_exists($class_action, 'chargementDataCplt')) {
            $action = $this->classAction($class_action);
            $action->chargementDataCplt($modification, $data, $formbuilder);
        }

        $form = $formbuilder->getForm();
        $form->handleRequest($this->requete->getRequest());
        if ($form->isSubmitted()) {

            //  Verification complémentaire
            if (method_exists($class_action, 'controleCplt')) {
                $action = $this->classAction($class_action);
                $action->controleCplt($form, $data, $modification);
            }

            if ($form->isValid()) {
                $args_rep0 = ['objet' => $objet];
                //  Traitement et sauvegarde des données

                if (method_exists($class_action, 'formSave')) {
                    $action = $this->classAction($class_action);
                    $args_rep0 += $action->formSave($form, $data, $modification);
                } else {
                    $args_rep0['id'] = $this->formSave_default($objet, $form, $data, $modification);
                }
            }
        }

        $args_rep += $args_rep0 ?? [];
        $class_argstwig = '\\App\\Form\\ArgsTwig\\' . $object_classe_form . 'ArgsTwig';
        if (method_exists($class_argstwig, 'getArgs')) {
            $argstwig = $this->classArgsTwig($class_argstwig);
            $args_rep += $argstwig->getArgs();
        }


        // Envoi  de la reponse
        if ($reponse) {
            return $this->reponse_formulaire($form, $args_rep);
        } else {
            return [$objet_data, $form, $args_rep];
        }
    }

    function classAction($class_action)
    {
        return new $class_action($this->requete, $this->em, $this->sac, $this->suc, $this->log);
    }

    protected function formSave_default($objet, $form, $objet_data, $modification)
    {
        $em = $this->em;
        $class_action = 'App\\Action\\' . camelize($objet) . 'Action';
        if (method_exists($class_action, 'form_save_cplt')) {
            $action = $this->classAction($class_action);
            $action->form_save_cplt($objet_data, $modification, $form);
        }
        $em->persist($objet_data);
        $em->flush();
        $this->log($modification ? 'MOD' : 'NEW', $objet, $objet_data);
        if (method_exists($class_action, 'form_save_after_flush')) {
            $action = $this->classAction($class_action);
            $action->form_save_after_flush($objet_data, $modification, $form);
        }
        return $objet_data->getPrimaryKey();
    }

    function classArgsTwig($class_argstwig)
    {
        return new $class_argstwig($this->container->get('router'));
    }

    function edit_defaut($objet_data, $args = [], $objet = null, $options = [], $reponse = true, $args_rep = [], $nom_class_form = null)
    {
        if (!$objet) {
            $objet = $this->sac->get('objet');
        }
        $descr_objet = $this->sac->descr($objet);
        // Controle autorisation
        if (!$this->gendarme->autoriser('OBJET_' . $descr_objet['groupe'] . '_MOD')) {
            return $this->redirect($this->generateUrl('erreur403'));
        }
        return $this->form_default(true, $objet_data, $args, $objet, $options, $reponse, $args_rep, $nom_class_form);
    }


    function xedit_defaut($objet_data, $options = [], $objet = null)
    {

        $args_rep = ['success' => false];
        $em = $this->em;
        $name = $this->requete->get('name');
        $value = $this->requete->get('value');
        $methode = 'set' . camelize($name);
        if (method_exists($objet_data, $methode)) {
            $objet_data->$methode($value);
            $em->persist($objet_data);
            $em->flush();
            $args_rep = ['success' => true];
        }

        return $this->json($args_rep);
    }

    function logo_defaut(Ged $ged, $objet_data, $options = [], $objet = null)
    {
        if (!$objet) {
            $objet = $this->sac->get('objet');
        }
        $descr_objet = $this->sac->descr($objet);
        if (!$this->gendarme->autoriser('OBJET_' . $descr_objet['groupe'] . '_MOD')) {
            return $this->redirect($this->generateUrl('erreur403'));
        }

        $args = ['id' . camelize($objet) => $objet_data->getPrimaryKey()];
        if ($this->requete->get('supprimer')) {
            $ged->supprimerDocuments($objet, $objet_data->getPrimaryKey(), 'logo');
            return $this->json([
                    'ok' => true,
                    'redirect' => $this->requete->get('redirect', $this->generateUrl($objet . '_show', $args)),
                    'message' => 'Le logo a bien été supprimé.'
                ]
            );
        }

        $form_logo = $this->generateFormUploadGed($objet, $objet_data->getPrimaryKey(), 'logo');
        if ($form_logo->isSubmitted() && $form_logo->isValid()) {
            return $this->json([
                    'ok' => true,
                    'redirect' => $this->requete->get('redirect', $this->generateUrl($objet . '_show', $args)),
                    'message' => 'Le logo a bien été modifié.'
                ]
            );
        }


        $args_twig = [];
        return $this->reponse_formulaire($form_logo, $args_twig);
    }


    function delete_defaut($objet_data, $objet = null, $args = [])
    {

        if (!$objet) {
            $objet = $this->sac->get('objet');
        }
        $descr_objet = $this->sac->descr($objet);
        if (!$this->gendarme->autoriser('OBJET_' . $descr_objet['groupe'] . '_SUP')) {
            return $this->redirect($this->generateUrl('erreur403'));
        }
        $data = ['ok' => false, 'message' => "Erreur dans la suppression"];

        if ($this->isCsrfTokenValid('delete-' . $objet . '-' . $objet_data->getPrimaryKey(), $this->requete->get('_token'))) {

            $em = $this->em;
            $id = $objet_data->getPrimaryKey();
            try {
                $class_action = 'App\\Action\\' . camelize($objet) . 'Action';
                if (method_exists($class_action, 'deleteBefore')) {
                    $action = $this->classAction($class_action);
                    $action->deleteBefore($objet_data);
                }
                $em->remove($objet_data);
                $em->flush();

                if (method_exists($class_action, 'deleteCplt')) {
                    $action = $this->classAction($class_action);
                    $action->deleteCplt();
                }

            } catch (Exception $exception) {
                $data['message'] .= $exception->getMessage();
                return $this->json($data);
            }
            $data['redirect'] = $this->requete->ouArgs('redirect', $args);
            if (empty($data['redirect'])) {
                $data['redirect'] = $this->generateUrl($objet . '_index');
            }
            $data['message'] = 'Le ' . $objet . ' a été supprimé';
            $data['ok'] = true;
            $this->log('DEL', $objet, $id);
        }


        return $this->json($data);
    }


    function show_defaut($objet_data, $objet = null)
    {
        if (!$objet) {
            $objet = $this->sac->get('objet');
        }
        $descr_objet = $this->sac->descr($objet);
        if (!$this->gendarme->autoriser('OBJET_' . $descr_objet['groupe'] . '_VIS')) {
            return $this->redirect($this->generateUrl('erreur403'));
        }
        $args_twig = [
            'objet_data' => $objet_data
        ];
        return $this->render($this->sac->fichier_twig(), $args_twig);
    }

    function testerRestriction($ob, $objet = null)
    {
        if (!$objet) {
            $objet = $this->sac->get('objet');
        }
        $db = $this->em->getConnection();
        $selecteur = new Selecteur($this->requete, $db, $this->sac, $this->suc);
        $selecteur->setObjet($objet);
        [$sql, $nb] = $selecteur->getSelectionObjetNb(['id' => $ob->getPrimaryKey()]);
        return $nb === 0;

    }


    function preparation_declencheur($tab_declencheur_possible)
    {
        $declencheur = $this->requete->get('declencheur');
        if (in_array($declencheur, $tab_declencheur_possible))
            return $declencheur;
        return '';
    }


    function histo_defaut($objet, $ob)
    {
        $args_twig = $this->suc->pref('timeline.accueil') +
            [
                'tab_operations' => $this->log->log_operations($objet, $ob->getPrimaryKey()),
                'class' => 'voir_plus'
            ];
        return $this->render('inclure/timeline.html.twig', $args_twig);
    }
}
