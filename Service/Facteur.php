<?php

namespace Declic3000\Pelican\Service;

use Doctrine\DBAL\Connection;
use Html2Text\Html2Text;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use TijsVerkoyen\CssToInlineStyles\CssToInlineStyles;
use Twig\Environment;
use Twig\Loader\FilesystemLoader;

class Facteur
{


    protected $db;
    protected $twig;
    protected $mailer;
    protected $sac;
    protected $redirect = false;
    protected $redirect_email;


    function __construct(Connection $db, Sac $sac, MailerInterface $mailer)
    {
        $this->db = $db;
        $this->mailer = $mailer;
        $this->sac = $sac;
        $rep = $this->sac->get('dir.root') . 'templates/mail/';
        if (file_exists($rep)) {
            @mkdir($rep);
        }
        $loader = new FilesystemLoader($rep);
        $this->twig = new Environment($loader, [
            'cache' => $this->sac->get('dir.cache') . 'mail/',
        ]);


    }


    function variable_entite($id_entite = 1)
    {

        $entite = $this->sac->tab('entite.' . $id_entite);
        return [
            'entite_nom' => $entite['nom'],
            'entite_email' => $entite['email'],
            'entite_adresse' => $entite['adresse'],
            'entite_codepostal' => $entite['codepostal'],
            'entite_ville' => $entite['ville'],
            'entite_pays' => $entite['pays'],
            'entite_telephone' => $entite['telephone'],
            'entite_url' => $entite['url']
        ];
    }


    function courriel_twig($to, $twig, $args_twig = [], $options = '')
    {
        $template = $this->twig->load($twig . '.html.twig');
        $sujet = trim((string)$template->renderBlock('sujet', $args_twig));
        $html = $template->renderBlock('contenu', $args_twig);
        return $this->courriel($to, $sujet, $html, $options);
    }

    /**
     * @throws TransportExceptionInterface
     */
    function courriel($to, $sujet, $html, $options = '', $from = null)
    {


        $conf = $this->sac->conf('email');
        $txt = new Html2Text($html);

        if ($this->redirect) {
            $to = $this->redirect_email;
        }
        $sujet = trim(((isset($conf['prefixe_sujet'])) ? $conf['prefixe_sujet'] . ' ' : '') . $sujet);

        $message = (new Email())
            ->subject($sujet)
            ->to($to);

        if (!empty($from)) {
            $message->from($from);
        } else if (isset($conf['from']) && !empty($conf['from'])) {
            $message->from($conf['from']);
        }

        if (preg_match_all('/<img[^>]* src="([^\"]*)"/', (string)$html, $output_array)) {
            foreach ($output_array[1] as $indice => $url) {
                if (!empty($url)) {

                    if ($url_real = realpath($url)) {
                        $file_info = pathinfo($url_real);
                        $mime = getMimeTypes($file_info['extension']);
                        $cid = 'image' . $indice;
                        $message->embedFromPath($url_real, $cid, $mime);
                        $html = str_replace($url, $cid, $html);
                    }
                }
            }
        }

        if (isset($options['css'])) {
            $cssToInlineStyles = new CssToInlineStyles();
            $html = $cssToInlineStyles->convert(
                $html,
                $options['css']
            );
        }

        $message->html($html)
            ->text($txt->getText());

        if (isset($options['pieces_jointes'])) {

            $tab_pj = (is_array($options['pieces_jointes'])) ? $options['pieces_jointes'] : [$options['pieces_jointes']];
            foreach ($tab_pj as $k => $pj) {
                if (is_int($k)) {
                    $k = basename((string)$pj);
                }
                $message->attachFromPath($pj, $k);
            }
        }

        $this->mailer->send($message);

        if (!empty($conf['email_copie'])) {
            $message->to($conf['email_copie']);
            $this->mailer->send($message);
        }

        return true;
    }

    function preparer_piece_jointe_avant_envoi($tab_ged)
    {


        $tab = [];
        foreach ($tab_ged as $ged) {
            $temp_path = __DIR__ . '../../var/';
            $file = $ged->saveFile(__DIR__ . '../../var/cache/');
            $nom_fichier = $ged->getNom() . '.' . $ged->getExtension();
            $tab[$nom_fichier] = $temp_path . '/' . $file;
        }
        return $tab;

    }


}
