<?php

namespace Declic3000\Pelican\Service;
/////////////////////////////////////////////////////////////////////
// outils pour stoker et destocker les préférences dans la BDD

use DateTime;
use Doctrine\DBAL\Connection;

class Preferenceur
{

    protected $systeme_pref;
    protected $table_sys;
    protected $utilise_entite;
    protected $db;


    public function __construct(Sac $sac, Connection $db)
    {
        $conf_app = $sac->get('app');
        if (empty($conf_app)) {
            $sac->initSac();
            $conf_app = $sac->get('app');
        }
        $this->utilise_entite = $conf_app['utilise_entite'] ?? false;
        $this->systeme_pref = $conf_app['systeme_pref'] ?? [];
        $this->table_sys = $conf_app['table_sys'] ?? [];
        $this->db = $db;
    }

    /** Mise à jour ou création(absence) de l'enregistrement opérateur et/ou entite la création se refere à l'enregistrement maitre en cas d'absense
     * @param $nom
     * @param $valeur
     *
     */
    function ecrire_pref($chemin, $valeur, $id_utilisateur, $id_entite = null)
    {
        if ($this->systeme_pref === 'col') {
            $tab_pref = $this->lire_pref($id_utilisateur);
            $tab_pref = dessinerUneBranche($tab_pref, $chemin, $valeur);
            $tab_sys_pref = $this->table_sys['preference'];
            $this->db->update($tab_sys_pref['table_sql'], [$tab_sys_pref['col_value'] => json_encode($tab_pref)], [$tab_sys_pref['col_id_user'] => $id_utilisateur]);
        } elseif ($this->systeme_pref === 'table') {

            $tab_clef = explode('.', (string)$chemin);
            $clef = $tab_clef[0];
            $db = $this->db;
            $info_table = $this->table_sys['preference'];
            $where[] = $info_table['col_name'] . '= ' . $db->quote($clef);
            $where[] = $info_table['col_id_user'] . ' = ' . $id_utilisateur;

            if ($this->utilise_entite) {
                if ($id_entite) {
                    $where[] = 'id_entite = ' . $id_entite;
                } else {
                    $where[] = 'id_entite is NULL';
                }
            }

            $preference_existante = $db->fetchAssociative('select * from ' . $info_table['table_sql'] . ' where ' . implode(' AND ', $where));
            if (!$preference_existante) { // creation à partir de la valeur par defaut

                $preference_default = $this->lire_pref_generique()[$clef] ?? [];
                $date_courante = new DateTime();
                $tab_data = [
                    $info_table['col_name'] => $clef,
                    $info_table['col_id_user'] => $id_utilisateur,
                    'created_at' => $date_courante->format('Y-m-d H:i:s'),
                    'updated_at' => $date_courante->format('Y-m-d H:i:s')
                ];
                if ($this->utilise_entite && $id_entite) {
                    $tab_data['id_entite'] = $id_entite;
                }
                if ($preference_default) {
                    $tab_data[$info_table['col_value']] = json_encode($preference_default);
                }
                $db->insert($info_table['table_sql'], $tab_data);
                $preference_existante = $tab_data;
                $id_pref = $db->lastInsertId();

            } else {
                $id_pref = $preference_existante[$info_table['cle']];
            }
            $preference_valeur = json_decode($preference_existante[$info_table['col_value']] ?? '[]', true);
            array_shift($tab_clef);
            $tab_data = [];
            if (empty($tab_clef)) {
                $tab_data[$info_table['col_value']] = $valeur;
            } else {
                $noeud = implode('.', $tab_clef);
                $tab_data[$info_table['col_value']] = dessinerUneBranche($preference_valeur, $noeud, $valeur);
            }
            $tab_data[$info_table['col_value']] = json_encode($tab_data[$info_table['col_value']]);
            $db->update($info_table['table_sql'], $tab_data, [$info_table['cle'] => $id_pref]);
        }

        return true;
    }

    function lire_pref(?int $id_utilisateur = null, $chemin = "")
    {
        $tab = [];
        $info_table = $this->table_sys['preference'];
        if (!empty($info_table)) {
            if ($this->systeme_pref === 'table') {
                if ($id_utilisateur > 0) {
                    $where = $info_table['col_id_user'] . ' =' . ((int)$id_utilisateur);
                } else {
                    $where = $info_table['col_id_user'] . ' IS NULL';
                }
                $tab_pref = $this->db->fetchAllAssociative('select ' . $info_table['col_name'] . ',' . $info_table['col_value'] . ' from ' . $info_table['table_sql'] . ' WHERE ' . $where);
                $col_name = $info_table['col_name'];
                foreach ($tab_pref as $p) {
                    $tab[$p[$col_name]] = json_decode((string)$p[$info_table['col_value']], true);
                }
            } else {
                $tab_pref = $this->db->fetchAssociative('select ' . $info_table['col_value'] . ' from ' . $info_table['table_sql'] . ' WHERE ' . $info_table['col_id_user'] . ' =' . ((int)$id_utilisateur));
                $tab = json_decode((string)$tab_pref[$info_table['col_value']], true);
            }
            $tab = detecte_datetime($tab);
            if ($chemin) {
                $tab = tableauChemin($tab, $chemin);
            }
        }
        return $tab;
    }

    function lire_pref_generique($chemin = null)
    {
        if ($this->systeme_pref === 'table') {
            $tab = $this->lire_pref(null, $chemin);
        } else {
            $info_table = $this->table_sys['config'];
            $tab_pref = $this->db->fetchAssociative('select ' . $info_table['col_value'] . ' from ' . $info_table['table_sql'] . ' WHERE ' . $info_table['col_name'] . ' = ' . $this->db->quote('preference_par_defaut'));
            $tab = json_decode((string)$tab_pref[$info_table['col_value']], true);
            if ($chemin) {
                $tab = tableauChemin($tab, $chemin);
            }
        }
        return $tab;
    }


}
