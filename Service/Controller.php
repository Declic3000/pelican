<?php

namespace Declic3000\Pelican\Service;

use Declic3000\Pelican\Component\Form\FormFactory;
use Declic3000\Pelican\Component\Table\TableSimple;
use Declic3000\Pelican\Tache\Tache;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Contracts\Translation\TranslatorInterface;


class Controller extends AbstractController
{
    protected $em;
    protected $sac;
    protected $suc;
    protected $log;
    protected $requete;
    protected $translator;
    protected $gendarme;
    protected $facteur;
    protected $formFactory;


    function __construct(EntityManagerInterface $em, Requete $requete, Sac $sac, Suc $suc, LogMachine $log, Facteur $facteur, TranslatorInterface $translator, Gendarme $gendarme, FormFactory $formFactory)
    {
        $this->em = $em;
        $this->log = $log;
        $this->sac = $sac;
        $this->suc = $suc;
        $this->requete = $requete;
        $this->translator = $translator;
        $this->gendarme = $gendarme;
        $this->formFactory = $formFactory;
        $this->facteur = $facteur;
        $this->sac->completeContexteRequete($this->requete);

    }


    function log($sufixe,
                 $objet = '',
                 $objet_data_ou_id = null,
                 $prefixe = '',
                 $observation = '',
                 $date_param = '',
                 $id_utilisateur = -1)
    {
        if ($this->log->estActif()) {
            return $this->log->add(
                $sufixe,
                $objet,
                $objet_data_ou_id,
                $prefixe,
                $observation,
                $date_param,
                $id_utilisateur
            );
        }
        return true;

    }

    function reponse_erreur($message)
    {
        return $this->render('page_erreur.html.twig', ['message' => $message]);
    }


    function getSession(): SessionInterface
    {
        return $this->requete->getRequest()->getSession();
    }


    function tab($var)
    {
        return $this->sac->tab($var);
    }

    function conf($var, $defaut = '')
    {
        return $this->sac->conf($var, $defaut);
    }

    /**
     * reponse_formulaire
     * @param $form
     * @param array $args
     * @param string $fichier_twig
     * @return string|JsonResponse|RedirectResponse
     */
    function reponse_formulaire($form, $args = [], $fichier_twig = '')
    {
        $message = '';
        // Preparation de la reponse
        if ($form->isSubmitted() && $form->isValid()) {

            $ok = true;
            $objet = $args['objet'] ?? $this->sac->get('objet');
            $modification = !empty($this->sac->get('id')) || (isset($args['modification']) && $args['modification']);
            $code_mes = (($objet) ? $objet . '_' : '') . (($modification) ? 'modifier' : 'ajouter');
            if ($this->sac->get('action') != '' || $objet == '') {
                $code_mes = 'enregistrement';
            }

            if (isset($args['message'])) {
                $message = $this->translator->trans($args['message']);
            } elseif (!isset($args['sans_message'])) {
                $code_mes .= '_ok';
                $message = $this->translator->trans($code_mes);
            }

            if (isset($args['message_cplt'])) {
                $message .= $args['message_cplt'];
            }
            $declencheur = $args['declencheur'] ?? null;
            if (!isset($args['url_redirect'])) {

                $id = $args['id'] ?? null;
                $redirect = $this->requete->get('redirect');
                $params = $args['params'] ?? [];
                if (!empty($declencheur)) {
                    $params['declencheur'] = $declencheur;
                }
                if (!empty($redirect)) {
                    $args['url_redirect'] = $redirect;
                } else {
                    $id ??= $this->sac->get('id');

                    if ($id) {
                        $page = ($this->sac->get('type_page') === 'edit' or $this->sac->get('type_page') === 'new') ? $objet . '_show' : $this->sac->get('page');
                        $params[$this->sac->descr($objet . '.cle') . ''] = $id;
                        $args['url_redirect'] = $this->generateUrl($page, $params);
                    } else {
                        $page = ($objet) ? $objet . '_index' : $this->sac->get('page');
                        if ($this->sac->get('page') !== 'preference_edit')
                            $args['url_redirect'] = $this->generateUrl($page, $params);
                    }
                }
                // $args['url_redirect'] = composer_url_redirect($id,$declencheur);
            } elseif ($args['url_redirect'] == '') {
                unset($args['url_redirect']);
            }
        } else {

            $ok = false;
            unset($args['url_redirect']);
            if (isset($args['message'])) {
                $message = $args['message'];
            }

            if ($form->isSubmitted()) {
                $message = $this->translator->trans('erreur_saisie_formulaire');
                $form->addError(new FormError($message));
            }

            if (isset($args['flashbag_erreur'])) {
                $this->addFlash('warning', $this->translator->trans($args['flashbag']));
            }

            if ($fichier_twig == '') {
                $fichier_twig = $this->sac->fichier_twig('', null, true);
            }

            $args_twig = $args;
            $args_twig['form'] = $form->createView();
            $html = $this->renderView($fichier_twig, $args_twig);
        }

        // Envoi de la reponse
        $reponse = [];
        if ($this->sac->get('ajax')) {

            $reponse['ok'] = $ok;
            if (isset($message) && !empty($message)) {
                $reponse['message'] = $message;
            }
            if (isset($html) || isset($args['html'])) {
                $reponse['html'] = $args['html'] ?? $html ?? '';
            }
            if (isset($args['url_redirect'])) {
                $reponse['redirect'] = $args['url_redirect'];
            }
            if (isset($args['declencheur_js'])) {
                $reponse['declencheur_js'] = $args['declencheur_js'];
            }
            if (isset($args['vars'])) {
                $reponse = array_merge($reponse, $args['vars']);
            }
            return new JsonResponse($reponse);
        }

        if ($ok) {
            $this->addFlash('success', $message);
            if (isset($args['url_redirect'])) {
                return $this->redirect($args['url_redirect']);
            }
        }
        return new Response($html ?? '');
    }

    function trans($chaine, $variables = [])
    {
        return $this->translator->trans($chaine, $variables);
    }

    function descr($var)
    {
        return $this->sac->descr($var);
    }

    function generateObjetExt($ob)
    {
        $classname = $ob::class;
        $objet = substr($classname, strrpos($classname, '\\') + 1);

        $class_action = '\\App\\EntityExtension\\' . camelize($objet) . 'Ext';

        return new $class_action($ob, $this->sac, $this->em);
    }

    /**
     * {@inheritdoc}
     */
    public function generateForm($name, $type = FormType::class, $data = null, array $options = [], $modif = true, $args = [])
    {
        return $this->formFactory->generateForm($name, $type, $data, $options, $modif, $args);
    }

    function generateFormUploadGed($objet, $id_objet, $usage = 'pj', $multiple = false, $type_formulaire = null)
    {
        $nom = 'upload_' . $objet . '_' . $id_objet . '_' . $usage;
        $form = $this->generateFormUpload($nom, $type_formulaire);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $ged = new Ged($this->em, $this->sac);
                $uploader = new Uploader($ged, $this->sac, $this->requete);
                $uploader->traitement_form_ged($nom, $id_objet, $objet, $usage, $multiple);
            }
        }
        return $form;
    }

    function generateFormUpload($nom, $type_formulaire = null)
    {

        $options = ['attr' => ['data-url' => $this->generateUrl('upload', ['nom' => $nom])]];
        if ($type_formulaire) {
            $builder = $this->formFactory->pregenerateForm($nom, $type_formulaire, null, $options, true);
        } else {
            $builder = $this->formFactory->pregenerateForm($nom, FormType::class, null, $options, true);
            $builder->add('document', FileType::class, ['label' => 'Pieces jointes', 'attr' => ['class' => 'jq-ufs']]);

        }
        $form = $builder->add('submit', SubmitType::class,
            ['label' => 'Enregistrer', 'attr' => ['class' => 'btn-primary']])
            ->getForm();
        $form->handleRequest($this->requete->getRequest());
        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function pregenerateForm($name, $type = FormType::class, $data = null, array $options = [], $modif = true, $args = [])
    {
        return $this->formFactory->pregenerateForm($name, $type, $data, $options, $modif, $args);
    }

    /**
     * @param string $objet
     * @param array $options_table
     * @param null|string $type_table
     * @return TableSimple
     */
    function createTable(string $objet, array $options_table = [], ?string $type_table = null)
    {

        $csrf = $this->container->get('security.csrf.token_manager');
        $type_table ??= $objet;
        $nom_class = 'App\\Component\\Table\\' . camelize($type_table) . 'Table';
        return new $nom_class($this->requete, $this->em, $this->sac, $this->suc, $csrf, $this->gendarme, $options_table);

    }

    /**
     * @return Tache
     */
    function createTache($nom, $avancement = [], $args = []): Tache
    {

        $nom_class = 'App\\Tache\\' . camelize($nom) . 'Tache';
        if (property_exists($nom_class, 'log')) {
            if (property_exists($nom_class, 'formbuilder')) {
                $tache = new $nom_class($this->sac, $this->suc, $this->em, $this->facteur, $this->log, $this->container->get('router'), $this->formFactory, $this->requete);
            } else {
                $tache = new $nom_class($this->sac, $this->suc, $this->em, $this->facteur, $this->log, $this->container->get('router'));
            }
        } else {
            $tache = new $nom_class($this->sac, $this->em);
        }
        $tache->tache_init($avancement, $args);
        return $tache;
    }

    function createTacheDiffere($fonction, $descriptif, $args)
    {

        $robotinit = new Robotinit($this->em, $this->sac);
        $robotinit->tache_ajouter($fonction, $descriptif, $args, $this->suc->get(), $this->suc->pref());
        return true;
    }

    function pref($var, $defaut = '')
    {
        return $this->suc->pref($var, $defaut);
    }

    protected function redirection(string $route, array $parameters = [], $flash_type = null, $flash_message = null): Response
    {

        if ($flash_type) {
            $this->addFlash($flash_type, $flash_message);
        }
        $url_redirect = $this->generateUrl($route, $parameters);
        if ($this->requete->estAjax()) {
            return $this->json(['ok' => true, 'message' => 'redirection', 'redirect' => $url_redirect]);
        }
        return $this->redirect($url_redirect, 302);
    }


}
