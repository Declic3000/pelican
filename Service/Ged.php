<?php

namespace Declic3000\Pelican\Service;

use App\Entity\Document;
use App\Entity\DocumentLien;
use Doctrine\ORM\EntityManagerInterface;
use PHPImageWorkshop\ImageWorkshop;
use RuntimeException;
use Symfony\Component\HttpFoundation\HeaderUtils;
use Symfony\Component\HttpFoundation\Response;


class Ged
{

    protected $chargeur;
    protected $em;
    protected $db;
    protected $path_output;
    protected $path_image;
    protected $path_cache;
    protected $fichier_ou_base;


    function __construct(EntityManagerInterface $em, Sac $sac)
    {

        $this->em = $em;
        $this->db = $em->getConnection();
        $this->chargeur = new Chargeur($this->em);
        $dir = $sac->get('dir.root');
        $this->fichier_ou_base = $sac->get('app.ged.fichier_ou_base');
        $this->path_output = $dir . 'var/output';

        if (!file_exists($this->path_output)) {
            if (!mkdir($concurrentDirectory = $this->path_output) && !is_dir($concurrentDirectory)) {
                throw new RuntimeException(sprintf('Directory "%s" was not created', $concurrentDirectory));
            }
        }

        // si stockage fichier
        if (!$this->fichier_ou_base) {
            $this->path_image = $dir . 'images/';
            if (!file_exists($this->path_image)) {
                if (!mkdir($concurrentDirectory = $this->path_image) && !is_dir($concurrentDirectory)) {
                    throw new RuntimeException(sprintf('Directory "%s" was not created', $concurrentDirectory));
                }
            }
        }
        $this->path_cache = $sac->get('dir.cache') . 'image/';
        if (!file_exists($this->path_cache)) {
            if (!mkdir($concurrentDirectory = $this->path_cache) && !is_dir($concurrentDirectory)) {
                throw new RuntimeException(sprintf('Directory "%s" was not created', $concurrentDirectory));
            }
        }


    }

    function enregistre_fichiers($tab_fichier, $id_objet, $objet_nom = 'individu', $utilisation = "", $multiple = false)
    {
        $ok = false;
        if (isset($tab_fichier[0])) {

            $document_existant = null;
            if (!$multiple) {
                $lien = $this->em->getRepository(DocumentLien::class)->findOneBy(['objet' => $objet_nom, 'idObjet' => $id_objet]);
                if ($lien)
                    $document_existant = $lien->getDocument();
            }


            foreach ($tab_fichier as $fichier) {
                $modif_ged = true;
                if ((!$document_existant) || $multiple) {
                    $modif_ged = false;
                    $document = new Document();
                } else {
                    $this->nettoyage_cache($document_existant);
                    $document = $document_existant;
                }
                $this->setFile($document, $fichier);
                $this->em->persist($document);
                $this->em->flush();

                if (!$modif_ged && $document->getIdDocument()) {
                    $this->document_lier($document->getIdDocument(), $objet_nom, $id_objet, $utilisation);
                }
                $ok = true;
            }
        }
        return $ok;
    }


    function getDocument($objet, $id_objet, $utilisation = null)
    {

        $tab_filtre = [
            'objet' => $objet,
            'idObjet' => $id_objet
        ];
        if ($utilisation) {
            $tab_filtre['utilisation'] = $utilisation;
        }
        $lien = $this->em->getRepository(DocumentLien::class)->findOneBy($tab_filtre);
        if ($lien) {
            return $lien->getDocument();
        }
    }

    function nettoyage_cache($document)
    {
        $rep = [$this->path_output . '/', $this->path_cache];
        foreach ($rep as $image_dir) {
            $tmp_name = $image_dir . md5((string)$document->getIdDocument()) . '-*' . '.*';
            foreach (glob($tmp_name) as $filename) {
                if (file_exists($filename)) {
                    unlink($filename);
                }
            }
        }
        return true;
    }

    function setFile($document, $fichier)
    {

        $path_parts = pathinfo((string)$fichier);
        $document->setExtension($path_parts['extension']);
        $document->setNom($path_parts['filename']);

        if ($this->fichier_ou_base) {
            $document->setImage(file_get_contents($fichier));
            $this->em->persist($document);
            $this->em->flush();
        } else {
            $this->em->persist($document);
            $this->em->flush();
            $nom_fichier_dest = $this->path_image . $document->getPrimaryKey() . '.' . $path_parts['extension'];
            if (file_exists($nom_fichier_dest)) {
                unlink($nom_fichier_dest);
            }
            copy($fichier, $nom_fichier_dest);
        }
        $this->nettoyage_cache($document);
        return true;
    }

    function document_lier($id_document, $objet, $id_objet, $utilisation = null)
    {
        $document = $this->chargeur->charger_objet('document', $id_document);
        if ($document) {
            $where = ['document' => $document, 'objet' => $objet, 'idObjet' => $id_objet];
            if ($utilisation) {
                $where['utilisation'] = $id_objet;
            }
            $tab_lien = $this->em->getRepository(DocumentLien::class)->findBy($where);
            if (empty($tab_lien)) {
                $ged_liens = new DocumentLien();
                $ged_liens->setObjet($objet);
                $ged_liens->setIdObjet($id_objet);
                $ged_liens->setDocument($document);
                $ged_liens->setUtilisation($utilisation);
                $this->em->persist($ged_liens);
                $this->em->flush();
            }
        }
        return true;
    }

    function getDocuments($objet, $id_objet, $utilisation = null)
    {

        $tab_filtre = [
            'objet' => $objet,
            'idObjet' => $id_objet
        ];
        if ($utilisation) {
            $tab_filtre['utilisation'] = $utilisation;
        }
        $liens = $this->em->getRepository(DocumentLien::class)->findBy($tab_filtre);
        $tab = [];
        foreach ($liens as $lien) {
            $tab[] = $lien->getDocument();
        }
        return $tab;

    }

    function image_preparation($objet, $id_objet, $utilisation, $taille)
    {

        $doclien = $this->em->getRepository(DocumentLien::class)->findOneBy(['utilisation' => $utilisation, 'objet' => $objet, 'idObjet' => $id_objet]);
        if ($doclien) {
            $document = $doclien->getDocument();
            $image_dir = $this->path_output . '/';
            if ($taille) {
                $image_dir = $this->path_output . '/';
            }
            if ($document) {

                $nom_fichier = $this->saveFile($document, $image_dir, $taille ?: 'original');
                $dimension_image = getimagesize($nom_fichier);
                $largeur = $dimension_image[0];
                $hauteur = $dimension_image[1];
                $layer = ImageWorkshop::initFromPath($nom_fichier);
                if ($largeur > $hauteur) {
                    $layer->resizeInPixel($taille, null, true);
                } else {
                    $layer->resizeInPixel(null, $taille, true);
                }
                $image = $layer->getResult("ffffff");
                $nom_fichier_jpg = change_extension($nom_fichier, 'jpg');
                imagejpeg($image, $nom_fichier_jpg, 95);
                /*if ($nom_fichier_jpg!==$nom_fichier){
                    unlink($nom_fichier);
                }*/
                return $nom_fichier_jpg;
            }
            exit();
        }
        return false;
    }

    function saveFile($document, $repertoire = '', $taille = 'original')
    {

        if ($repertoire === '') {
            $repertoire = $this->path_output;
        }
        $tmp_name = md5((string)$document->getIdDocument()) . '-' . $taille . '.' . $document->getExtension();
        $repertoire .= (str_ends_with((string)$repertoire, '/')) ? '' : '/';

        if (!file_exists($repertoire . $tmp_name)) {
            if (!file_exists($repertoire)) {
                if (!mkdir($repertoire) && !is_dir($repertoire)) {
                    throw new RuntimeException(sprintf('Directory "%s" was not created', $repertoire));
                }
            }
            if ($this->fichier_ou_base) {

                file_put_contents($repertoire . $tmp_name, $document->getImage());
            } else {

                copy($this->path_image . '/' . $document->getPrimaryKey() . '.' . $document->getExtension(), $repertoire . $tmp_name);
            }
        }
        return $repertoire . $tmp_name;
    }

    function sendFile(Document $document, $inline = false)
    {

        $headers = [
            'Content-type: ' . getMimeTypes($document->getExtension()),
            'Content-Disposition: ' . (($inline) ? 'inline' : 'attachment') . '; filename="' . $document->getNom() . '.' . $document->getExtension() . '"'
        ];
        if ($this->fichier_ou_base) {
            $response = new Response(stream_get_contents($document->getImage()), 200, $headers);
        } else {

            $f = fopen($this->path_image . '/' . $document->getPrimaryKey() . '.' . $document->getExtension(), 'r');
            $response = new Response(stream_get_contents($f), 200, $headers);

        }
        $disposition = HeaderUtils::makeDisposition(
            HeaderUtils::DISPOSITION_ATTACHMENT,
            $document->getNom() . '.' . $document->getExtension()
        );
        $response->headers->set('Content-Disposition', $disposition);
        return $response;
    }

    function preparer_piece_jointe_avant_envoi($tab_document)
    {
        $tab = [];
        foreach ($tab_document as $document) {
            $file = $this->saveFile($document, $this->path_output);
            $nom_fichier = $document->getNom() . '.' . $document->getExtension();
            $tab[$nom_fichier] = $file;
        }
        return $tab;
    }

    function enregistrer($path_fichier, $tab_liens = [], $utilisation = '')
    {

        $document = new Document();
        $this->setFile($document, $path_fichier);
        $this->em->persist($document);
        foreach ($tab_liens as $objet => $liens) {
            if (!is_array($liens)) {
                $liens = [$liens];
            }
            foreach ($liens as $id_objet) {
                $ged_liens = new DocumentLien();
                $ged_liens->setObjet($objet);
                $ged_liens->setIdObjet($id_objet);
                $ged_liens->setDocument($document);
                $ged_liens->setUtilisation($utilisation);
                $this->em->persist($ged_liens);
            }
        }
        $this->em->flush();
        return $document->getPrimaryKey();

    }

    function document_delier($id_document, $objet, $id_objet)
    {
        $document = $this->chargeur->charger_objet('document', $id_document);
        if ($document) {
            $tab_lien = $this->em->getRepository(DocumentLien::class)->findBy(['document' => $document, 'objet' => $objet, 'idObjet' => $id_objet]);
            foreach ($tab_lien as $lien) {
                $this->em->remove($lien);
            }
        }
        $this->em->flush();
        return true;
    }


    function supprimerDocuments($objet, $id_objet, $utilisation = null)
    {
        $tab_where = ['objet' => $objet, 'idObjet' => $id_objet];
        if ($utilisation) {
            $tab_where['utilisation'] = $utilisation;
        }
        $tab_docs = $this->em->getRepository(DocumentLien::class)->findBy($tab_where);
        foreach ($tab_docs as $doc) {
            $doc = $doc->getDocument();
            if ($doc) {
                $this->em->remove($doc);
            }
        }
        $this->em->flush();
        return true;
    }

    /**
     * @return string
     */
    public function getPathImage(): string
    {
        return $this->path_image;
    }


}

