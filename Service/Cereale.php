<?php

namespace Declic3000\Pelican\Service;

use Doctrine\Common\Annotations\AnnotationReader;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactory;
use Symfony\Component\Serializer\Mapping\Loader\AnnotationLoader;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class Cereale
{


    protected $serializer;

    public function __construct()
    {

        $classMetadataFactory = new ClassMetadataFactory(new AnnotationLoader(new AnnotationReader()));
        $normalizer = new ObjectNormalizer($classMetadataFactory);
        $this->serializer = new Serializer([$normalizer]);


    }


    public function normalize($obj)
    {

        return $this->serializer->normalize($obj, null, ['groups' => 'write']);
    }

    public function denormalize($data, $class_name)
    {

        return $this->serializer->denormalize($data, $class_name, null, ['groups' => 'write']);
    }

}
