<?php

namespace Declic3000\Pelican\Service;

use DateInterval;
use DateTime;
use Symfony\Contracts\Translation\TranslatorInterface;

class Bigben
{

    protected $trans;

    public function __construct(TranslatorInterface $translator)
    {
        $this->trans = $translator;
    }

    /**
     * date_periode
     * @param $date_debut
     * @param $date_fin
     * @return string
     */
    function date_periode($date_debut, $date_fin)
    {


        if (is_a($date_debut, 'DateTime') && is_a($date_fin, 'DateTime')) {

            if ($date_debut->format('d/m') == '01/01' && $date_fin->format('d/m') == '31/12' && $date_debut->format('Y') == $date_fin->format('Y')) {
                return $date_debut->format('Y');
            } elseif ($date_debut->format('d/m') == '01/07' && $date_fin->format('d/m') == '30/06' && $date_debut->format('Y') == ($date_fin->format('Y') - 1)) {
                return $date_debut->format('Y') . '-' . $date_fin->format('Y');
            } elseif ($date_debut->format('d/m') == '01/09' && $date_fin->format('d/m') == '31/08' && $date_debut->format('Y') == ($date_fin->format('Y') - 1)) {
                return $date_debut->format('Y') . '-' . $date_fin->format('Y');
            } elseif ($date_debut->format('d') == '01' && $date_fin->format('d') > 27 && $date_debut->format('Y') == $date_fin->format('Y')) {
                return $this->trans->trans(strtolower($date_debut->format('F'))) . $this->trans->trans(' à ') . $this->trans->trans(strtolower($date_fin->format('F'))) . ' ' . $date_fin->format('Y');
            } elseif ($date_debut->format('d') == '01' && $date_fin->format('d') > 27) {
                return $this->trans->trans('De ') . $date_debut->format('F Y') . $this->trans->trans(' à ') . $date_fin->format('F Y');
            }

            return $this->trans->trans('Du ') . $date_debut->format('d/m/Y') . $this->trans->trans(' au ') . $date_fin->format('d/m/Y');
        }
        return '';

    }


    function date_en_clair($objetdate, $format = '')
    {

        if (is_string($objetdate)) {
            $objetdate = new DateTime($objetdate);
        }
        $mois = $this->mois_en_clair($objetdate->format('m'));
        $jour = $objetdate->format('j');
        $annee = $objetdate->format('Y');
        $retour = match ($format) {
            'ma' => $this->trans->trans(strtolower((string)$mois)) . '-' . $annee,
            'm' => $this->trans->trans(strtolower((string)$mois)),
            default => $jour . ' ' . strtolower((string)$mois) . ' ' . $annee,
        };
        return $retour;

    }

    function mois_en_clair($mois)
    {
        $mois_nom = ["", "Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre"];
        return $mois_nom[(int)$mois];
    }

    function duree_microseconde($debut, $commentaire)
    {
        return '<br>' . ((int)((microtime(true) - $debut) * 1000)) . ' millisecondes ' . $commentaire;
    }

    function calculeDateDebut($date_dernier_sr = null, $retard_jours = 0, $temp = '0,M,12,1,1,fin')
    {

        // la date origine est J+1 de la derniere cotisation de l'entite ou la date du jour en cas d'absence.
        $date_jour = new DateTime();

        if ($date_dernier_sr === null) {
            $date_ori = DateTime::createFromFormat('Y-m-d', '1900-01-01');
            $premiere = true;
        } else {
            $date_ori = clone $date_dernier_sr;
            $premiere = false;
        }
        $format1 = 'Y-m-d';
        $format2 = "";
        if ($date_ori <= $date_jour or $premiere) {

            //           echo ' date du jour inferieure à la date de la prochaine cotisation on ne modife rien '.date_format($date_jour, 'Y-m-d').'**<BR>';
            if (substr_count((string)$temp, ',') < 3) {
                $temp = '0,M,12,1,1,fin';
            }
            [$periodique, $unite, $duree, $mois_debut, $jour_debut] = explode(',', (string)$temp);
            $jour_debut = str_pad($jour_debut, 2, '0', STR_PAD_LEFT);
            $mois_debut = str_pad($mois_debut, 2, '0', STR_PAD_LEFT);
            $duree_jour = 0;
            switch ($periodique) {
                case 0: //pas de duree
                    $duree_jour = 0;
                    $format1 = 'Y';
                    $format2 = '-01-01';
                    break;
                case 1: //fixe

                    if ($unite === 'M') {
                        if (((int)$mois_debut) === 0) {
                            $format1 = 'Y-m';
                            $format2 = '-' . $jour_debut;
                        } else {
                            $format1 = 'Y';
                            $format2 = '-' . $mois_debut . '-' . $jour_debut;
                        }
                        $duree_jour = 30.5 * ((int)$duree);
                    } elseif ($unite === 'D') {
                        $format1 = 'Y-m';
                        $format2 = '-' . $jour_debut;
                        $duree_jour = $duree;
                    } else { // Année
                        $format1 = 'Y';
                        $format2 = '-' . $mois_debut . '-' . $jour_debut;
                        $duree_jour = 365 * ((int)$duree);

                    }
                    break;
                case 2: //glissant
                    $duree_jour = 365 * ((int)$duree);
                    if ($unite === 'M') {
                        $duree_jour = 30.5 * ((int)$duree);
                    } elseif ($unite === 'D') {
                        $duree_jour = ((int)$duree);
                    }
                    $format1 = 'Y-m-d';
                    $format2 = '';
                    break;
            }

            $format1 = $format1 . $format2;
            $format = $format1 . ' 00:00:00';

            if ($periodique > 0) {
                $ajout = 'P' . $duree . $unite;
                // echo " format :" . $format . '  ajoute  :' . $ajout . ' Duree en jours :' . $duree_jour;
                $date_debut_periode = DateTime::createFromFormat('Y-m-d H:i:s', $date_jour->format($format));
                if ($date_debut_periode >= $date_jour) {
                    $interval = new DateInterval($ajout);
                    $date_debut_periode = $date_debut_periode->sub($interval);
                }
                // echo '<BR> Date debut periode :'.$date_debut_periode->format('Y-m-d');
                $retard_reel = $date_ori->diff($date_jour)->format('%a');
                if ($duree_jour > 0) {
                    $retard_modulo = ($retard_reel % $duree_jour);
                    $retard_nbperiode = (int)($retard_reel / $duree_jour);
                } else {
                    $retard_modulo = $retard_reel;
                    $retard_nbperiode = (int)$retard_reel;
                }


                if ($retard_modulo >= $retard_jours) {
                    $retard_nbperiode++;
                }

                $retard_debut_periode = $date_debut_periode->diff($date_jour)->format('%a');
                if ($retard_debut_periode > $retard_jours) {
                    $interval = new DateInterval($ajout);
                    $date_debut_periode = $date_debut_periode->add($interval);
                }
                $date_debut = $date_debut_periode;

            } else {
                $date_debut = clone $date_jour;
            }
        } else {
            $date_debut = clone $date_ori;
        }
        return $date_debut;
    }

    function calculeDateFin($date, $temp = '0,M,12,1,1,fin')
    {
        $date_fin = clone $date;
        if (is_a($date_fin, 'DateTime')) {
            [$periodique, $unite, $duree, $mois_debut, $jour_debut, $truc] = explode(',', (string)$temp);
            if ($periodique == 1 || $periodique == 2) {
                $ajout = 'P' . $duree . $unite;
                $date_fin = $date_fin->add(new DateInterval($ajout))->sub(new DateInterval('P1D'));
            } else {
                $date_fin = new DateTime('3000-01-01');
            }
        }
        return $date_fin;
    }

    function calcule_DateDebut_origine_DateFin($date_ori, $temp = '0,M,12,1,1,fin')
    {
        $date_jour = new DateTime();
        if ($date_ori == null) {
            return false;
        }
        $format1 = 'Y-m-d';
        $format2 = "";
        if (substr_count((string)$temp, ',') < 3) {
            $temp = '0,M,12,1,1,fin';
        }
        [$periodique, $unite, $duree, $mois_debut, $jour_debut] = explode(',', (string)$temp);
        $jour_debut = str_pad($jour_debut, 2, '0', STR_PAD_LEFT);
        $mois_debut = str_pad($mois_debut, 2, '0', STR_PAD_LEFT);
        switch ($periodique) {
            case 0: //pas de duree
                $format1 = 'Y';
                $format2 = '-01-01';
                break;
            case 1: //fixe
                $format1 = 'Y';
                $format2 = '-' . $mois_debut . '-' . $jour_debut;
                if ($unite === 'M') {
                    if (intval($mois_debut) == 0) {
                        $format1 = 'Y-m';
                        $format2 = '-' . $jour_debut;
                    } else {
                        $format1 = 'Y';
                        $format2 = '-' . $mois_debut . '-' . $jour_debut;
                    }
                } elseif ($unite === 'D') {
                    $format1 = 'Y-m';
                    $format2 = '-' . $jour_debut;
                }
                break;
            case 2: //glissant
                if ($unite === 'M') {
                    $duree_jour = 30.5 * ((int)$duree);
                } elseif ($unite === 'D') {
                }
                $format1 = 'Y-m-d';
                $format2 = '';
                break;
        }
        $format1 = $format1 . $format2;
        $format = $format1 . ' 00:00:00';
        if ($periodique > 0) {
            $ajout = 'P' . $duree . $unite;
            $date_debut = $date_ori->sub(new DateInterval($ajout))->add(new DateInterval('P1D'));
        } else {
            $date_debut = clone $date_jour;
        }
//    echo ' date calculée :'.$date_debut->format('d-m-Y');
        return $date_debut;
    }

    function transforme_date($date)
    {
        return preg_replace('#(\d{2})/(\d{2})/(\d{4})#', '$3-$2-$1', (string)$date);
    }

}
