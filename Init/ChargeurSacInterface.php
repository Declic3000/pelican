<?php

namespace Declic3000\Pelican\Init;

use App\Init\VariablesApp;
use App\Init\VariablesPreference;
use Declic3000\Pelican\Service\Bigben;
use Declic3000\Pelican\Service\ChargeurDb;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Exception;

class ChargeurSacInterface
{

    protected $db;
    protected $chargeurdb;
    protected $description;
    protected $bigben;
    protected $vars_app;


    public function __construct(Connection $db, Bigben $bigben)
    {
        $this->db = $db;
        $this->chargeurdb = new ChargeurDb($db);
        $this->bigben = $bigben;
        $this->vars_app = $this->chargement_variables_app();
    }

    function chargement_variables_app()
    {
        return (new VariablesApp())->getVariables();
    }


    /**
     * @throws Exception
     */
    function remplir(): array
    {
        // chargement des descriptions completes des objets
        $this->description = $this->description_complete();
        $options = $this->getOptions();
        $tab_vars_env = $this->chargementVarsEnv();


        $tab_config = [];
        if ($options['table_sys']['config']) {
            $tab_config = $this->chargement_config();
        }

        $tab_pref = [];
        if ($options['systeme_pref'] === 'table') {
            if ($options['table_sys']['preference']) {
                $tab_pref = [
                    'preference_meta' => $this->chargement_preference(),// chargement des preferences par défaut
                    'preference' => $this->chargement_preference_defaut(),// chargement des preferences par défaut
                ];
            }
        } else {
            $tab_pref = [
                'preference' => $tab_config['config']['preference_par_defaut'] ?? [],// chargement des preferences par défaut
            ];
        }
        $tab_table = $this->chargement_table();

        $tab_app_suppl = [
            'liens_role_objet' => $this->liste_liens_objet_role()
        ];
        if ($options['utilise_entite'] && isset($options['entite'])) {
            $tab_app_suppl['multi_entite'] = count($tab_table['entite']) > 1;
        }
        return [
                'description' => $this->description,
                'app' => $tab_vars_env + $options + $tab_app_suppl,
                'table' => $tab_table
            ]
            + $tab_config
            + $tab_pref;

    }

    /**
     * description_complete
     *
     * @return array
     * @throws Exception
     */
    function description_complete()
    {

        $options = $this->getOptions();

        $description = $this->liste_des_objets();
        ksort($description);
        $tab_alias = [];
        $sm = $this->db->createSchemaManager();

        foreach ($description as $objet => &$do) {

            $do['prefixe'] ??= $options['prefixe_table'];
            $do['prefixe_'] = $do['prefixe'] . (($do['prefixe'] == '') ? '' : '_');
            $do['objet'] = $objet;


            $do['source'] = $options['template_sous_dossier'] ? ($do['source'] ?? $do['prefixe'] . '/') : '';
            $do['objet_action'] ??= $objet;

            if (isset($do['alias'])) {
                $do['objet'] = $do['alias'] ?? $objet;
                if (isset($do['alias_valeur'])) {
                    $do['alias_valeur_class'] ??= $do['alias_valeur'];
                    $tab_alias[$do['alias']][$objet] = $do['alias_valeur'];
                }
            }

            $do['phpname'] ??= camelize($do['objet']);


            $do['nom_sql'] ??= $do['objet'];

            if (!isset($do['table_sql'])) {
                $do['table_sql'] = $do['prefixe_'] . $do['nom_sql'] . (($options['suffixe_table_avec_s']) ? 's' : '');
            }


            if ($options['cle_id']) {
                $do['cle'] ??= 'id';
                $do['cle_sql'] ??= 'id';
            } else {
                $do['cle'] ??= 'id' . $do['phpname'];
                $do['cle_sql'] ??= 'id_' . ($do['nom_sql'] ?? $do['objet']);
            }


            $do['affichage'] ??= $options['col_affichage_defaut'];

            $tab_colonnes_exclues_tri = [];
            $className = "\App\Entity\\" . camelize($objet);
            if (class_exists($className)) {
                if (defined("$className::COLONNES_EXCLUES_TRI")) {
                    $tab_colonnes_exclues_tri = ($className)::COLONNES_EXCLUES_TRI;
                }
            }


            $columns = $sm->listTableColumns($do['table_sql']);
            $tab_cols = [];
            foreach ($columns as $column) {
                $tab_cols[$column->getName()] = $column->getType()->getBindingType() . "";
            }
            $do['colonnes'] = $tab_cols;
            $do['colonnes_tri'] = array_keys(array_diff_key($tab_cols, array_flip($tab_colonnes_exclues_tri)));
            $do['colonnes_query_exclus'] ??= [];
            $do['colonnes_interdites'] ??= [];
            $do['colonnes_extra'] ??= [];
            // le nom des fichiers sources
            $tab_action = ['show' => 'show', 'index' => 'index', 'new' => 'new', 'edit' => 'edit', 'table' => '_table'];
            if (isset($do['nom_ecran'])) {
                $tab_action = ['voir' => $do['nom_ecran'], 'liste' => $do['nom_ecran'], 'new' => $do['nom_ecran'], 'edit' => $do['nom_ecran'], 'table' => $do['nom_ecran']];
            }

            if (!isset($do['twig'])) {
                $path = realpath(__DIR__ . '/../../..');

                foreach ($tab_action as $k_action => $v_action) {
                    $do['twig'][$k_action] = $this->source($path, '/templates/', $do['source'], $do['objet'], $v_action, '.html.twig', $objet);
                }
            }


            if (isset($do['choix_colonne'])) {
                $cc = $do['choix_colonne'];
                $do['choix_colonne'] = (is_array($cc)) ? $cc : [$cc];
            } else {
                $champs_ordre_possible = array_values(array_intersect(array_keys($do['colonnes']), [
                    'code',
                    'nom',
                    'libelle',
                    'libelle1'
                ]));
                $do['choix_colonne'] = empty($champs_ordre_possible) ? [
                    ''
                ] : [
                    $champs_ordre_possible[0]
                ];
            }


            if (!isset($do['choix_ordre'])) {
                $champs_ordre_possible = array_intersect(array_keys($do['colonnes']), [
                    'nom',
                    'libelle',
                    'libelle1',
                    'code',
                    'label',
                    'name'
                ]);
                $do['choix_ordre'] = array_shift($champs_ordre_possible);
            }


            $do['champs_nom'] ??= $do['choix_ordre'];
            // Applique ou non les autorisations
            $do['autorisation_lecture'] ??= true;
            $do['autorisation_ecriture'] ??= true;

            if ($do['prefixe'] === 'geo') {
                $do['autorisation_lecture'] = false;
            }

            $do['nomcplt'] = ' '; // a justifier l'espace et la présence nomcplt = nomcourt ou nom sur 6 ou id en dernier ressort
            $do['log'] ??= strtoupper(substr($objet, 0, 3));

            if (!isset($do['groupe'])) {
                if (in_array($do['prefixe'], ['com', 'sys', 'doc', 'geo'])) {
                    $do['groupe'] = $do['prefixe'];
                } else {
                    $do['groupe'] = 'generique';
                }
            }

            ksort($do);
        }


        foreach ($tab_alias as $obj => $v) {
            if (isset($description[$obj])) {
                $description[$obj]['tab_alias'] = $v;
            }
        }
        return $description;
    }

    function getOptions()
    {
        $options = $this->vars_app['options'] ?? [];
        $options_default = [
            'prefixe_table' => '',
            'suffixe_table_avec_s' => true,
            'cle_id' => false,
            'template_sous_dossier' => false,
            'utilise_log' => true,
            'utilise_entite' => true,
            'utilise_droit' => true,
            'systeme_pref' => 'table',
            'col_affichage_defaut' => 'nom',
            'ged' => [
                'utilisation' => true,
                'fichier_ou_base' => true // stockage dans true=>bdd, false=>fichier
            ],
            'log' => [
                'utilise_liens' => false,
                'code' => [
                    'CON' => 'config'
                ]
            ],
            'tache' => [
                'robot' => false,
                'init' => []
            ],
            'table_sys' => [
                "config" => [
                    "table_sql" => 'sys_configs',
                    "cle" => 'id_config',
                    "col_name" => 'nom',
                    "col_value" => 'variables',
                    "col_id_user" => 'id_utilisateur',
                    "col_observation" => 'observation'
                ],
                "preference" => [
                    "table_sql" => 'sys_preferences',
                    "cle" => 'id_preference',
                    "col_name" => 'nom',
                    "col_value" => 'variables',
                    "col_id_user" => 'id_utilisateur',
                    "col_observation" => 'observation'
                ],
                "utilisateur" => [
                    "objet" => 'utilisateur',
                    "table_sql" => 'sys_utilisateurs',
                    "cle_sql" => 'id_utilisateur'
                ]
            ]
        ];
        return table_merge($options_default, $options);
    }

    function liste_des_objets()
    {
        return $this->vars_app['objets'] ?? [];
    }

    /**
     * source
     *
     * @param   $path
     * @param   $rep
     * @param    $source
     * @param     $objet
     * @param    $action
     * @param     $sufixe
     * @param     $objet_origine
     * @return string
     */
    function source($path, $rep, $source, $objet, $action, $sufixe, $objet_origine = "", $type_vue = 'objet')
    {
        if (file_exists($path . $rep . $source . $objet_origine . '/' . $action . $sufixe)) {
            return $objet_origine . '/' . $action;
        } elseif (file_exists($path . $rep . $source . $objet . '/' . $action . $sufixe)) {
            return $objet . '/' . $action;
        }
        return $type_vue . '/' . $action;
    }

    function chargementVarsEnv()
    {

        $vars_env = $this->getVarsEnv();
        $tab_vars_env = [];
        if (is_array($vars_env)) {
            foreach ($vars_env as $key => $nom_vars) {
                $tab_vars_env[$key] = $_ENV[$nom_vars] ?? '';
            }
        }
        return $tab_vars_env;
    }

    function getVarsEnv()
    {
        return $this->vars_app['vars_env'] ?? [];
    }

    function chargement_config()
    {
        $tab = [];
        $tab_entite = [];
        $options = $this->getOptions() ?? [];
        $info_table = $options['table_sys']['config'];
        if ($info_table) {
            $tab_config = $this->charger_table('config', [
                'table' => $info_table['table_sql'],
                'cle' => $info_table['cle']
            ]);
            $col_name = $info_table['col_name'];
            foreach ($tab_config as $c) {
                if (isset($c['id_entite']))
                    $tab_entite[$c['id_entite']][$c[$col_name]] = objectToArray(json_decode((string)$c['valeur']));
                else
                    $tab[$c[$col_name]] = objectToArray(json_decode((string)$c['variables']));
            }
        }
        return [
            'config' => $tab,
            'config_entite' => $tab_entite
        ];
    }

    function charger_table($objet, $options = [])
    {
        $options['cle'] ??= $this->description[$objet]['cle_sql'];
        $table = $options['table'] ?? $this->description[$objet]['table_sql'];
        return $this->chargeurdb->charger_table($table, $options);
    }

    function chargement_preference()
    {

        $var_pref = new VariablesPreference();
        $tab_prefs = $var_pref->getVariables();
        $tab_objet = array_keys($this->liste_des_objets());
        foreach ($tab_objet as $o) {
            if (!isset($tab_prefs[$o])) {
                $tab_prefs[$o] = $var_pref->prepare_modele_objet($o);
            }
        }
        return $tab_prefs;
    }

    function chargement_preference_defaut()
    {
        $tab = [];
        $options = $this->getOptions() ?? [];
        if ($options['systeme_pref'] === 'table') {
            $info_table = $options['table_sys']['preference'];
            if ($info_table) {
                $tab_pref = $this->charger_table('preference', [
                    'table' => $info_table['table_sql'],
                    'cle' => $info_table['cle'],
                    'where' => $info_table['col_id_user'] . ' is null'
                ]);
                $col_name = $info_table['col_name'];
                foreach ($tab_pref as $p) {
                    $tab[$p[$col_name]] = objectToArray(json_decode((string)$p['variables']));
                }
            }
        }
        return $tab;
    }

    function chargement_table(): array
    {
        return [];
    }

    function liste_liens_objet_role()
    {
        $tab_lien = $this->vars_app['liens_role_objet'] ?? [];
        $tab_temp = [];
        $tab_ope = ["VIS", "CRE", 'MOD', 'SUP'];
        foreach ($tab_lien as $role => $groupe) {
            foreach ($groupe as $g => $ope) {
                if (is_string($ope) && strtoupper($ope) === "ALL") {
                    $ope = $tab_ope;
                } else {
                    $ope = is_array($ope) ? $ope : [$ope];
                }
                foreach ($ope as $o) {
                    $tab_temp['ROLE_OBJET_' . $g . '_' . $o][] = $role;
                }
            }
        }
        return $tab_temp;

    }


}
