<?php

namespace Declic3000\Pelican\Init;

class VariablePreferenceInterface extends VariableInterface
{

    function prepare_modele_objet($objet, $relation = false)
    {
        $tmp = [
            'variables' => [
                'index' => $this->prepare_modele_page('index', $objet),
            ],
            'parametrage_par_entite' => true
        ];
        if (!$relation) {
            $tmp['variables']['show'] = $this->prepare_modele_page('show', $objet);
        }
        return $tmp;
    }

    function prepare_modele_page($type, $objet = '')
    {


        $modele_page = [
            'index' => ['datatable' => $this->prepare_modele_datatable('index', $objet)],
            'show' => ['affichage_histo' => true] // afficher le bloc historique sur la fiche de l'objet
        ];
        return $modele_page[$type];

    }

    function prepare_modele_datatable($type, $objet = '')
    {
        $modele_datatable = [
            'index' => [
                'nb_ligne' => 20,
                'colonne_affichage' => ['type_champs' => 'colonne_affichage', 'valeur' => null],
            ],
            'show' => [
                'affichage' => true,
                'nb_ligne' => 5,
                'colonne_affichage' => ['type_champs' => 'colonne_affichage', 'valeur' => null]
            ]
        ];
        $temp = $modele_datatable[$type];
        if ($objet != '') {
            $temp['tri_par_defaut'] = ['type_champs' => 'tri_' . $objet, 'valeur' => null];
        }
        return $temp;
    }

}
