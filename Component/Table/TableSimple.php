<?php

namespace Declic3000\Pelican\Component\Table;


use Declic3000\Pelican\Component\Filtre\Filtre;
use Declic3000\Pelican\Component\Tri\Tri;
use Declic3000\Pelican\Service\Requete;
use Declic3000\Pelican\Service\Sac;
use Declic3000\Pelican\Service\Selecteur;
use Declic3000\Pelican\Service\Suc;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;

class TableSimple extends Selecteur
{

    public const COLONNES = [
        'id' => [],
        'nom' => []
    ];
    public const COLONNES_SET = [];
    public const FILTRES = [
        'premier' => [
            'search' => ['ajouter_recherche']
        ]
    ];
    protected $em;
    protected $objet_pref;
    protected $colonnes = [];
    protected $chemin_pref = null;
    protected $id_table = null;
    protected $nb_ligne = 10;
    protected $tri_par_defaut = [];
    protected $tri = [];
    protected $trier_par = [];
    protected $tri_combinaison = [];
    protected $liste_selection_defaut = [];
    protected $liste_selection = [];
    protected $enregistrement_selection = false;
    /**
     * @var Filtre|null
     */
    protected $fg = null;

    /**
     * @var Tri|null
     */
    protected $tg = null;
    protected $filtres = [];
    protected $mode_affichage = 'datatable';
    protected $colonnes_set = 'default';
    protected $type_table;
    protected $colonnes_exclues = [];
    protected $url_args = [];
    protected $options_js = [];
    protected $csrf = null;


    public function __construct(Requete $requete, EntityManagerInterface $em, Sac $sac, Suc $suc, ?CsrfTokenManagerInterface $csrf, array $options = [])
    {
        parent::__construct($requete, $em->getConnection(), $sac, $suc);
        if (empty($this->objet_pref)) {
            $this->objet_pref = $this->objet;
        }
        $this->csrf = $csrf;
        $this->em = $em;

        if (!$this->chemin_pref) {
            $this->chemin_pref = $this->sac->get('objet') . '.' . $this->sac->get('type_page') . '.datatable';
            if ($this->sac->get('type_page') === 'show') {
                $this->chemin_pref = $this->chemin_pref . "_" . $this->objet_pref;
            }
        }
        if (empty($this->id_table)) {
            $this->id_table = str_replace('.', '_', $this->chemin_pref);
        }

        $pref = $suc->pref($this->chemin_pref);
        $this->nb_ligne = $pref['nb_ligne'] ?? $this->nb_ligne;

        $this->tri_par_defaut = $pref['tri_par_defaut'] ?? $this->tri_par_defaut;
        $this->liste_selection_defaut = $pref['liste_selection_defaut'] ?? [];
        $class = static::class;
        $this->type_table = substr(static::class, strrpos($class, '\\') + 1, -5);
        $this->url_args = $options['url_args'] ?? [];

        $this->initOptions($options);
        $this->initColonnes();
        $this->initTriGenerator();
        $this->initTri();
        $this->initFiltreGenerator();
        $this->initFiltres();

    }

    protected function initOptions($options)
    {

        $options_diponible = ['mode_affichage', 'colonnes_set', 'colonnes_exclues', 'nb_ligne', 'options_js', 'tri'];

        foreach ($options_diponible as $od) {
            if ($val = $this->requete->get($od)) {
                $this->$od = $val;
            } elseif (isset($options[$od])) {
                $this->$od = $options[$od];
            }
        }
    }

    protected function initColonnes()
    {
        if ($this->mode_affichage === 'datagrid') {
            $this->colonnes = $this->initColonnesGrid();
        } else {
            if ($this->colonnes_set && isset($this::COLONNES_SET[$this->colonnes_set])) {
                $tab_colonne = $this::COLONNES_SET[$this->colonnes_set];
            } else {
                $tab_colonne = $this::COLONNES;
            }

            foreach ($this->colonnes_exclues as $k) {
                unset($tab_colonne[$k]);
            }
            $this->colonnes = $this->datatable_colonne_visible($tab_colonne);
            $this->colonnes = $this->datatable_complete_liste_colonne($this->colonnes);

        }
    }

    protected function initColonnesGrid()
    {
        $nomclass = '\App\Entity\\' . ucfirst((string)$this->objet);
        $tab_cols0 = $nomclass::$allowedFields;
        $tab_cols = [];
        foreach ($tab_cols0 as $cols) {
            $tab_cols[$cols] = [
                'title' => $cols
            ];
        }
        $tab_cols['id' . ucfirst((string)$this->objet)] = [
            'title' => 'id'
        ];
        $tab_colonne = $this->datatable_complete_liste_colonne($this::COLONNES);
        $tab_cols = array_merge($tab_cols, $tab_colonne);

        return $this->datatable_complete_liste_colonne($tab_cols);
    }

    /**
     * datatable_complete_liste_colonne
     *
     * @param array $tab_colonne
     * @return mixed
     */
    public function datatable_complete_liste_colonne(array $tab_colonne)
    {

        return static::datatable_complete_liste_colonne_static($tab_colonne);
    }

    public static function datatable_complete_liste_colonne_static(array $tab_colonne)
    {

        foreach ($tab_colonne as $k => &$col) {
            if (!is_array($col))
                $col = [];
            if (!isset($col['name'])) {
                $col['name'] = $k;
            }
            if (!isset($col['title'])) {
                $col['title'] = $k;
            }
            if (!isset($col['responsivePriority'])) {
                $col['responsivePriority'] = 1;

            }
        }
        return $tab_colonne;
    }

    protected function datatable_colonne_visible($tab_colonne)
    {

        $colonne_masquee = $this->suc->pref($this->chemin_pref . '.colonne_affichage');
        if (is_string($colonne_masquee)) {
            $colonne_masquee = explode(':', $colonne_masquee);
        }
        foreach ($tab_colonne as $k => &$col) {
            if (in_array($k, $colonne_masquee ?? [])) {
                $col['visible'] = false;
            }


        }
        return $tab_colonne;
    }

    function initTriGenerator()
    {


        $classname = '\\App\\Component\\Tri\\' . camelize($this->objet) . 'Tri';
        if (class_exists($classname))
            $this->tg = new $classname($this->requete, $this->sac);
        else {
            $this->tg = new Tri($this->requete, $this->sac);
        }
        return true;
    }

    protected function initTri()
    {
        $critere_tri_init = empty($this->tri) ? $this->tri_par_defaut : $this->tri;
        $this->tg->init($this->objet, $critere_tri_init, $this->trier_par, $this->colonnes, $this->tri_combinaison);

    }

    function initFiltreGenerator()
    {

        $classname = '\\App\\Component\\Filtre\\' . camelize($this->objet) . 'Filtre';
        if (class_exists($classname))
            $this->fg = new $classname($this->requete, $this->sac, $this->db, $this->liste_selection_defaut);
        else {
            $this->fg = new Filtre($this->requete, $this->sac, $this->db, $this->liste_selection_defaut);
        }
        return true;
    }

    protected function initFiltres()
    {
        $this->filtres = $this::FILTRES;
        return $this->filtres;
    }

    public static function epurer_colonne_triable($tab_col)
    {
        $tab = [];

        foreach ($tab_col as $k => $col) {
            if (!(isset($col['orderable']) && !$col['orderable'])) {
                $tab[$k] = $col['title'] ?? '';
            }
        }
        return $tab;
    }

    function dataliste_rechercher($limit = 20)
    {
        [$tab_id, $nb_total] = $this->dataliste_preselection();


        $tab = $this->getAllObjet($tab_id, 0, $limit);

        if ($this->mode_affichage === 'datagrid')
            $tab_data = $this->dataliste_preparation_datagrid($tab);
        else
            $tab_data = $this->dataliste_preparation($tab);
        $options_ligne = $this->dataliste_preparation_options_ligne($tab);
        return [
            $tab_data,
            $options_ligne,
            $nb_total
        ];
    }

    /**
     *
     * @param array $args
     * @param bool $tri
     * @param bool $limit
     *
     * @return array
     */
    function dataliste_preselection(array $args = [], bool $trier = true, bool $limiter = true)
    {
        if ($trier) {

            $pr = $this->sac->descr($this->objet . '.nom_sql');
            $cle = $this->sac->descr($this->objet . '.cle_sql');
            $tab_select_ajout = [$pr . '.' . $cle];
            $tab_tri = $this->tri_dataliste();
            $tab_colonne_triable = $this->getColonneTriable();
            foreach ($tab_tri as $col => $sens) {
                if (isset($tab_colonne_triable[$col]['select_ajout'])) {
                    $tab_select_ajout[] = $tab_colonne_triable[$col]['select_ajout'];
                }
            }

            [$from, $where] = $this->construireWhere($args);
            [$order, $tab_left] = $this->tri_sql($this->objet, $tab_tri);
            $from += $tab_left;
            $from = array_unique($from);
            [$sql, $nb_total] = $this->getRequeteObjetNb($from, $where, $tab_select_ajout);
            $sql .= $order;

        } else {
            [$sql, $nb_total] = $this->getSelectionObjetNb($args);
        }

        if ($limiter) {
            $start = $this->requete->ouArgs('start');
            $length = $this->requete->ouArgs('length');
            $sql .= ' LIMIT ' . intval($start) . ',' . (($length == "") ? 30 : $length);
        }

        $this->ajoute_debug($sql);
        $tab_id = $this->db->fetchAllAssociative($sql);
        $cle = $this->sac->descr($this->objet . '.cle_sql');
        $tab_id = table_simplifier($tab_id, $cle);
        return [
            $tab_id,
            $nb_total
        ];
    }

    public function tri_dataliste()
    {
        return $this->tg->tri_dataliste();
    }

    public function getColonneTriable()
    {
        return $this->tg->getColonneTriable();
    }

    /**
     * getAllObjet
     *
     * @param $tab_id
     * @param int|null $offset
     * @param int|null $limit
     *
     * @return mixed
     */
    function getAllObjet($tab_id, ?int $offset = 0, ?int $limit = null)
    {
        $cle = $this->sac->descr($this->objet . '.cle');

        $tab_object = $this->em->getRepository('\\App\\Entity\\' . $this->sac->descr($this->objet . '.phpname'))->findBy([$cle => $tab_id], [], $limit, $offset);

        $ti = array_flip($tab_id);
        $tab_r = [];
        while (!empty($tab_object)) {
            $ob = array_shift($tab_object);
            $tab_r[$ti[$ob->getPrimaryKey()]] = $ob;
        }
        ksort($tab_r);
        return $tab_r;
    }

    /**
     *
     * @param array $tab
     * @param bool $only_value
     *
     * @return array
     */
    function dataliste_preparation_datagrid(array $tab, bool $only_value = true)
    {
        return $this->dataliste_preparation($tab, $only_value);
    }

    /**
     * @param $tab
     * @param bool $only_data
     * @return array
     */
    function dataliste_preparation($tab, $only_data = true)
    {
        $tab_data = [];
        if (!empty($tab)) {
            $tab_data = $this->datatable_prepare_data($tab, $this->getColonnes());
        }
        return $tab_data;
    }

    /**
     * datatable_prepare_data
     *
     * @param array $tab_objets
     * @param array $tab_colonnes
     * @param bool $only_value
     * @return array
     */
    function datatable_prepare_data(array $tab_objets, array $tab_colonnes, $only_value = true)
    {
        $tab_data = [];

        foreach ($tab_objets as $o) {

            $data = $o->toArray();
            $temp = [];
            foreach ($tab_colonnes as $nom_col => $col) {
                if (isset($col['manuel']) && $col['manuel']) {
                    $temp[$nom_col] = '';
                } else {
                    if (strpos($nom_col, '.') > -1) {
                        $table = substr($nom_col, 0, strpos($nom_col, '.'));
                        $nom_fonction = 'get' . ucfirst($table);
                        if ($o2 = $o->$nom_fonction()) {
                            $nom_col_table = substr($nom_col, strpos($nom_col, '.') + 1);
                            $nom_fonction = 'get' . ucfirst($nom_col_table);
                            $temp[$nom_col] = $o2->$nom_fonction();
                        }
                    } else {
                        $val = '';
                        if (isset($data[$nom_col]) && !(isset($col['type']))) {

                            $val = $data[$nom_col];

                        } else {
                            $nfonc = 'get' . ucfirst($nom_col);
                            if (method_exists($o, $nfonc)) {
                                $val = $o->$nfonc();
                            } else {
                                $nfonc = 'is' . ucfirst($nom_col);
                                if (method_exists($o, $nfonc)) {
                                    $val = (bool)$o->$nfonc();
                                }
                            }

                        }

                        $format_date = (isset($col['type']) && $col['type'] === 'date-eu') ? 'd/m/Y' : 'd/m/Y H:i:s';

                        $temp[$nom_col] = (is_a($val, '\Datetime')) ? $val->format($format_date) : $val;


                    }
                    if (!isset($temp[$nom_col])) {
                        $temp[$nom_col] = '';
                    }
                }
            }
            if ($only_value) {
                $tab_data[] = array_values($temp);
            } else {
                $tab_data[] = $temp;
            }
        }
        return $tab_data;
    }

    public function getColonnes()
    {
        return $this->colonnes;
    }

    function dataliste_preparation_options_ligne($tab)
    {
        $tab_options = [];
        foreach ($tab as $i => $t) {
            $token = $this->csrf->getToken('delete-' . $this->objet_pref . '-' . $t->getPrimaryKey());
            $tab_options[$i]['token'] = $token->getValue();
        }
        return $tab_options;
    }

    /**
     *
     *
     * @return array
     */
    function objet_dataliste()
    {
        $this->liste_args_filtre();
        [$tab_id, $nb_total] = $this->dataliste_preselection($this->liste_selection);

        $tab = $this->getAllObjet($tab_id, 0, 99999999);

        if ($this->mode_affichage === 'datagrid') {
            $tab_data = $this->dataliste_preparation_datagrid($tab);
        } else {
            $tab_data = $this->dataliste_preparation($tab);
        }
        $options_ligne = $this->dataliste_preparation_options_ligne($tab);

        return $this->dataliste_envoi($tab_data, $nb_total, $options_ligne);
    }

    function liste_args_filtre()
    {
        $tab_champs_filtres = Filtre::liste_filtre($this->getFiltres());
        $tab_champs_filtres[] = 'query';
        $tab_requete = [];

        foreach ($tab_champs_filtres as $filtre) {
            $temp = $this->requete->get($filtre);
            if (!empty($temp)) {
                if ($filtre === 'search') {
                    if ($temp['value'] != '') {
                        $tab_requete[$filtre] = $temp;
                    }
                } else {
                    $tab_requete[$filtre] = $temp;
                }
            } else {
                $temp = $this->requete->get($filtre . '[]');

                if (!empty($temp)) {
                    $tab_requete[$filtre . '[]'] = $temp;
                }
            }

        }
        $this->liste_selection = $tab_requete;
        return $this->liste_selection;
    }

    public function getFiltres()
    {
        return $this->filtres;
    }

    /**
     *
     * @param array $tab_data
     * @param int $nb_total
     * @param array $options_ligne
     * @param array $options
     *
     * @return array
     */
    function dataliste_envoi(array $tab_data, int $nb_total, array $options_ligne = [], array $options = [])
    {
        return [
            'draw' => $this->requete->get('draw'),
            'recordsTotal' => $nb_total,
            'recordsFiltered' => $nb_total,
            'debug' => $this->affiche_debug(),
            'options_ligne' => $options_ligne,
            'data' => $tab_data,
            'options' => $options,
            'error' => ''
        ];
    }

    /**
     *
     * @param array $dyn
     * @param array $args
     * @param ?int $limit
     * @return array
     */
    public function export($args = [], ?int $limit = null)
    {

        [$tab_id, $nb_total] = $this->dataliste_preselection($args, true, false);
        if (!$limit) {
            $limit = 999999999999999;
        }
        $tab = $this->getAllObjet($tab_id, 0, $limit);
        array_walk($tab, fn(&$el) => $el->toArray());
        return $tab;
    }

    /**
     *
     * @param bool $dyn
     * @param array $args
     * @param ?int $limit
     * @return array
     */
    public function export_twig(bool $dyn = false, array $args = [], ?int $limit = null)
    {

        $result = $this->export_twig_args();
        $result['dyn'] = $dyn;

        if (!$dyn) {
            $this->requete->setBloquer(true);
            [$tab_id, $nb_total] = $this->dataliste_preselection($args, true, false);

            if (!$limit) {
                $limit = 999999999999999;
            }
            $tab = $this->getAllObjet($tab_id, 0, (int)$limit);
            $result['donnees'] = $this->dataliste_preparation($tab);
            $result['nb'] = $nb_total;
            $result['options_ligne'] = $this->dataliste_preparation_options_ligne($tab);
            $this->requete->setBloquer(false);
        } else {
            $result['filtres_statiques'] = $args;
        }

        return $result;
    }

    public function export_twig_args()
    {

        $result = [
            'colonnes' => $this->getColonnes(),
            'tab_tri' => [
                'courant' => table_indice($this->tri_dataliste()),
                'colonnes' => $this->getColonneTriable(),
                'combinaison' => $this->getTriCombinaison()
            ],
            'pref' => ['nb_ligne' => $this->nb_ligne],
            'options_js' => $this->options_js,
            'filtres' => $this->fullFiltres(),
            'querybuilder' => $this->querybuilder,
            'mode_affichage' => $this->mode_affichage,
            'colonnes_exclues' => $this->colonnes_exclues,
            'colonnes_set' => $this->colonnes_set,
            'type_table' => $this->type_table,
            'url_args' => $this->url_args,
            'selection' => $this->enregistrement_selection
        ];
        if ($this->mode_affichage === 'datagrid') {
            $result['template'] = $this->getObjet() . '.html';
        }
        return $result;
    }

    public function getTriCombinaison()
    {
        return $this->tg->getTriCombinaison();
    }

    public function fullFiltres()
    {
        return $this->fg->construireBarreFiltre($this->filtres);
    }

    protected function initFiltresValeur($tab_filtres)
    {
        return $tab_filtres;
    }


}
