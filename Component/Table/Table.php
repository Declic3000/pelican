<?php

namespace Declic3000\Pelican\Component\Table;

use Declic3000\Pelican\Service\Gendarme;
use Declic3000\Pelican\Service\Requete;
use Declic3000\Pelican\Service\Sac;
use Declic3000\Pelican\Service\Suc;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;

class Table extends TableSimple
{

    protected $session = null;
    protected $gendarme = null;


    public function __construct(Requete $requete, EntityManagerInterface $em, Sac $sac, Suc $suc, ?CsrfTokenManagerInterface $csrf = null, ?Gendarme $gendarme = null, array $options = [])
    {
        parent::__construct($requete, $em, $sac, $suc, $csrf, $options);
        $this->session = $requete->getRequest()->getSession();
        $this->gendarme = $gendarme;
    }


    function donneTriActif()
    {

        $tab_tri = $this->session->get('tri_courant_' . $this->id_table);
        if (empty($tab_tri)) {
            $tab_tri = $this->tri_par_defaut;
            if (!empty($tab_tri)) {
                if (strpos(',', (string)$tab_tri) != -1) {
                    $tab_tri = explode(',', (string)$tab_tri);
                } else {
                    $tab_tri = [$tab_tri];
                }
                $tab_tri_tmp = [];
                foreach ($tab_tri as $tri) {
                    $tmp = explode(' ', (string)$tri);
                    $tab_tri_tmp [$tmp[0]] = $tmp[1] ?? 'ASC';
                }
                $tab_tri = $tab_tri_tmp;
            }
        }
        $this->tri = $tab_tri;
        return $tab_tri;
    }


    /**
     *
     * @return array
     */
    function objet_dataliste()
    {
        $this->stocke_derniere_selection();
        return parent::objet_dataliste();
    }

    function stocke_derniere_selection()
    {
        $this->session->set('tri_courant_' . $this->id_table, $this->tg->tri_dataliste());
        $this->session->set('selection_courante_' . $this->id_table, $this->liste_args_filtre());
        $this->session->set('affichage_courant_' . $this->id_table, $this->requete->get('affichage'));
    }


    public function export_twig_args()
    {
        if ($this->session->has('selection_courante_' . $this->id_table)) {
            $tab_valeurs = $this->session->get('selection_courante_' . $this->id_table);
            $this->fg->setValeurParDefaut($tab_valeurs);
        }
        $tab = parent::export_twig_args();
        if ($this->session->has('tri_courant_' . $this->id_table)) {
            $tab['tri'] = $this->session->get('tri_courant_' . $this->id_table);
        }
        return $tab;

    }


}
