<?php

namespace Declic3000\Pelican\Component\Tri;

use Declic3000\Pelican\Service\Requete;
use Declic3000\Pelican\Service\Sac;

class Tri
{
    protected $sac;
    protected $requete;
    protected $objet;
    protected $critere_tri_init;
    protected $trier_complexe;
    protected $colonneTriable;
    protected $tri_combinaison;

    public function __construct(Requete $requete, Sac $sac)
    {
        $this->requete = $requete;
        $this->sac = $sac;
    }

    public function init($objet, $critere_tri_init, $trier_par_init, $colonnes, $tri_combinaison)
    {
        $this->objet = $objet;
        $this->critere_tri_init = $this->deserializeTri($critere_tri_init);
        // Ajout des colonnes issu de l'objet en cours
        $tab_colonnes = $this->sac->descr($this->objet . '.colonnes_tri');
        $tab_temp = [];
        if (!empty($tab_colonnes)) {
            foreach ($tab_colonnes as $col) {
                $tab_temp[camelize2($col)] = ['name' => $col];
            }
            // Ajout des colonnes issu des colonnes du tableau
            $tab_temp = array_merge($tab_temp, $this->reformate_colonne_trier_par($colonnes));
        } else {
            $tab_temp = $this->reformate_colonne_trier_par($colonnes);
        }

        $this->setColonneTriable($this->complete_trier_par(array_merge($trier_par_init, $tab_temp)));
        $this->setTriCombinaison($this->completetriCombinaison($tri_combinaison));
    }

    function deserializeTri($chaine_tri)
    {
        $tab_tri = [];
        if (!empty($chaine_tri)) {
            if (strpos(',', (string)$chaine_tri) != -1) {
                $trier_par = explode(',', (string)$chaine_tri);
            } else {
                $trier_par = [
                    $chaine_tri
                ];
            }
            foreach ($trier_par as $tri) {
                if (strpos((string)$tri, '-sens-') > -1) {
                    $temp = explode('-sens-', (string)$tri);
                    $tab_tri[$temp[0]] = $temp[1];
                }
                if (strpos((string)$tri, ' ') > -1) {
                    $temp = explode(' ', (string)$tri);
                    $tab_tri[$temp[0]] = $temp[1];
                }
            }
        }
        return $tab_tri;
    }

    /**
     *
     * @param array $colonnes
     * @return array
     */
    function reformate_colonne_trier_par(array $colonnes): array
    {
        $tab = [];
        foreach ($colonnes as $k => $colonne) {
            if (!(isset($colonne['orderable']) && $colonne['orderable'] == false)) {
                $tab[$k] = [
                    'name' => $colonne['title']
                ];
                if (strpos($k, '.')) {
                    $tab[$k]['liaisons'] = [substr($k, 0, strpos($k, '.'))];
                    $tab[$k]['champs'] = $k;
                }
            }
        }
        return $tab;
    }

    /**
     *
     * @param array $tab_trier_par
     * @return mixed
     */
    function complete_trier_par($tab_trier_par)
    {

        $last_liaison = $this->objet;
        foreach ($tab_trier_par as $k => &$tri) {
            if (!isset($tri['champs'])) {
                if (isset($tri['liaisons'])) {
                    $last_liaison = end($tri['liaisons']);
                }
                $tri['champs'] = $last_liaison . '.' . $k;
            }
        }

        return $tab_trier_par;
    }

    /**
     *
     * @param array $tab_tri_combinaison
     * @return array
     */
    function completeTriCombinaison($tab_tri_combinaison)
    {
        return $tab_tri_combinaison;
    }

    public function getColonneTriable()
    {
        return $this->colonneTriable;
    }

    public function setColonneTriable(mixed $colonneTriable): void
    {
        $this->colonneTriable = $colonneTriable;
    }

    /**
     * @return mixed
     */
    public function getTriCombinaison()
    {
        return $this->tri_combinaison;
    }

    public function setTriCombinaison(mixed $tri_combinaison): void
    {
        $this->tri_combinaison = $tri_combinaison;
    }

    /**
     * tri_dataliste
     *
     * @return array
     */
    function tri_dataliste()
    {
        $tab_tri = $this->critere_tri_init;
        $tab_select_ajout = [];
        $trier_par = $this->requete->ouArgs('trier_par');

        if (!empty($trier_par)) {
            $tab_tri = $this->deserializeTri($trier_par);
        } else {
            $tab_col = $this->requete->get('columns');
            $tab_order = $this->requete->get('order');

            if (is_array($tab_order) && !empty($tab_order)) {
                $tab_tri = [];
                foreach ($tab_order as $odr) {
                    $num_col = $odr['column'];
                    if (isset($tab_col[$num_col]['name']) && $tab_col[$num_col]['orderable']) {
                        $tab_tri[$tab_col[$num_col]['name']] = strtoupper((string)$odr['dir']);
                    }
                }
            }
        }
        return $tab_tri;
    }
}