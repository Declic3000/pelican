<?php

namespace Declic3000\Pelican\Component\Filtre;

use Declic3000\Pelican\Service\ChargeurDb;
use Declic3000\Pelican\Service\Requete;
use Declic3000\Pelican\Service\Sac;
use Doctrine\DBAL\Connection;

class Filtre
{
    protected $sac;
    protected $requete;
    protected $db;
    protected $valeur_par_defaut;
    protected $chargeur;

    public function __construct(Requete $requete, Sac $sac, Connection $db, $valeur_par_defaut = [])
    {
        $this->requete = $requete;
        $this->sac = $sac;
        $this->db = $db;
        $this->chargeur = new ChargeurDb($db);
        $this->setValeurParDefaut($valeur_par_defaut);
    }

    function setValeurParDefaut($valeur_par_defaut)
    {
        $this->valeur_par_defaut = $valeur_par_defaut;
    }

    public static function liste_filtre($tab_filtres)
    {

        if (isset($tab_filtres['premier'])) {
            $result = array_keys($tab_filtres['premier']);
            if (isset($tab_filtres['second'])) {
                foreach ($tab_filtres['second'] as $filtres) {
                    foreach ($filtres as $f) {
                        if (is_array($f)) {
                            $result = array_merge($result, array_keys($f));
                        }
                    }
                }
            }
            if (isset($tab_filtres['invisible'])) {
                $result = array_merge($result, $tab_filtres['invisible']);
            }
            return $result;
        }
    }

    public function construireBarreFiltre($filtres)
    {

        if (isset($filtres['premier'])) {
            foreach ($filtres['premier'] as $key => &$val) {
                $method = 'filtre_' . $val[0];
                if (method_exists($this, $method)) {
                    $val = $this->$method($val[1] ?? null);
                    if ($val === null) {
                        unset($filtres['premier'][$key]);
                    }
                } else {
                    unset($filtres['premier'][$key]);
                }
            }
        }
        if (isset($filtres['second'])) {
            foreach ($filtres['second'] as $key => &$panneau) {
                foreach ($panneau as $ke => &$colonne) {
                    if (is_array($colonne)) {
                        foreach ($colonne as $k => &$val) {
                            if (isset($val[0])) {
                                $method = 'filtre_' . $val[0];
                                if (method_exists($this, $method)) {
                                    $val = $this->$method($val[1] ?? null);
                                    if ($val === null) {
                                        unset($filtres['second'][$key][$ke][$k]);
                                    }
                                } else {
                                    unset($filtres['second'][$key][$ke][$k]);
                                }
                            }
                        }
                    }
                }
            }
        }
        return $filtres;
    }

    function filtre_avec_email()
    {
        return $this->filtre_serie('avec_emai', ['Avec email' => 'oui', 'Sans email' => 'non', 'Email incorrect' => 'npai', 'Email correct' => 'ok']);
    }

    function filtre_serie($nom_filtre, $tab_filtres)
    {
        $valeur_filtre = $this->requete->ouArgs($nom_filtre, $this->valeur_par_defaut);
        return $this->filtre_prepare_donnee($nom_filtre, $tab_filtres, $valeur_filtre);
    }

    function filtre_prepare_donnee($nom, $tab_valeur, $valeur, $class = 'list-inline')
    {

        $filtre = [
            'nom' => $nom,
            'class' => $class
        ];
        if ($tab_valeur and is_array($tab_valeur)) {

            foreach ($tab_valeur as $k => $val) {

                if (is_array($valeur)) {
                    $on = in_array($val, $valeur);
                } else {
                    $on = ($val == $valeur);
                }

                $filtre['options'][] = ['libelle' => $k, 'valeur' => $val, 'on' => $on];
            }
        } else {
            $checked = !($this->requete->ouArgs($nom) === null);
            $options = [['on' => $checked, 'valeur' => 'inverse', 'libelle' => $nom]];

            $filtre = [
                'id' => $nom,
                'nom' => $nom,
                'type' => 'checkbox',
                'options' => $options
            ];

        }
        return $filtre;
    }

    function filtre_avec_adresse()
    {
        return $this->filtre_serie('avec_adresse', ['Avec adresse' => 'oui', 'Adresse non renseigné' => 'non', 'Adresse non valide' => 'npai', 'Adresse valide' => 'valide']);
    }

    function filtre_avec_telephone()
    {
        return $this->filtre_serie('avec_telephone', ['Avec telephone fixe' => 'oui', 'Pas de téléphone fixe' => 'non', 'Numéro de téléphone incorrect' => 'npai']);
    }

    function filtre_avec_telephone_pro()
    {
        return $this->filtre_serie('avec_telephone_pro', ['Avec telephone pro' => 'oui', 'Pas de téléphone pro' => 'non', 'Numéro de téléphone pro incorrect' => 'npai']);
    }

    function filtre_avec_mobile()
    {
        return $this->filtre_serie('avec_mobile', ['Avec mobile' => 'oui', 'Pas de téléphone mobile' => 'non', 'Numéro de téléphone mobile incorrect' => 'npai']);
    }

    function filtre_ardent()
    {
        return $this->filtre_serie('ardent', ['Ardent' => 'ardent', 'Perdu de vue' => 'non', 'nouveau' => 'nouveau']);
    }

    function filtre_etat()
    {
        return $this->filtre_serie('etat', ['Ancien adhérent' => 'oui', 'Adhérent actuel' => 'non']);
    }

    function filtre_est_decede()
    {
        return $this->filtre_serie('est_decede', ['Vivant' => 'non', 'Décédé' => 'oui']);
    }

    function filtre_geo_commune()
    {
        return [
            'nom' => 'commune',
            'class' => 'autocomplete_select2 select2_autocomplete_commune',
            'options' => $this->charger_options('commune', 'id_commune as id,nom', 'id_commune'),
            'attr' => [
                'data-url' => '/geo/commune?action=autocomplete',
                'data-message' => 'aucune commune trouvée'
            ],
            'multiple' => true
        ];
    }

    function charger_options($objet, $champs, $where_champs)
    {
        $options = [];
        $code = $this->requete->ouArgs($objet, $this->valeur_par_defaut);
        if ($code) {
            $code = is_array($code) ? $code : [$code];
            $result = $this->chargeur->charger_where($this->sac->descr($objet . '.table_sql'), $champs, $where_champs . ' IN (\'' . implode('\',\'', $code) . '\')');
            $options = $this->filtre_options_select(table_colonne_cle_valeur($result, 'id', 'nom'), $code, true);
        }
        return $options;
    }

    function filtre_options_select($tab, $values, $complexe = false)
    {


        $values = empty($values) ? [] : (is_array($values) ? $values : [$values]);
        $t = [];
        foreach ($tab as $k => $val) {
            if ($complexe) {
                $t[$k] = ['libelle' => $val, 'valeur' => $k];
                if ((!empty($values)) && in_array($k, $values)) {
                    $t[$k]['on'] = true;
                }
            } else {
                $t[$val] = ['libelle' => $val, 'valeur' => $val];
                if ((!empty($values)) && in_array($k, $values)) {
                    $t[$val]['on'] = true;
                }
            }


        }
        return $t;
    }

    function filtre_geo_codepostal()
    {
        return [
            'nom' => 'codepostal',
            'class' => 'autocomplete_select2 select2_autocomplete_codepostal',
            'options' => $this->charger_options('codepostal', 'codepostal as id,nom', 'codepostal'),
            'attr' => [
                'data-url' => '/geo/codepostal?action=autocomplete',
                'data-message' => 'aucun code postal trouvée',
                'data-tags' => '1'
            ],
            'multiple' => true
        ];
    }

    function filtre_geo_arrondissement()
    {
        return [
            'nom' => 'arrondissement',
            'class' => 'autocomplete_select2 select2_autocomplete_arrondissement',
            'options' => $this->charger_options('arrondissement', 'id_arrondissement as id,nom', 'id_arrondissement'),
            'attr' => [
                'data-url' => '/geo/arrondissement?action=autocomplete',
                'data-message' => 'aucun arrondissement trouvé'
            ],
            'multiple' => true
        ];
    }

    function filtre_geo_departement()
    {
        return [
            'nom' => 'departement',
            'class' => 'autocomplete_select2 select2_autocomplete_departement',
            'options' => $this->charger_options('departement', 'code as id,nom', 'code'),
            'attr' => [
                'data-url' => '/geo/departement?action=autocomplete',
                'data-message' => 'aucun département trouvé'
            ],
            'multiple' => true
        ];
    }

    function filtre_geo_region()
    {
        return ['nom' => 'region',
            'class' => 'autocomplete_select2 select2_autocomplete_region',
            'options' => $this->charger_options('region', 'id_region as id,nom', 'id_region'),
            'attr' => [
                'data-url' => '/geo/region?action=autocomplete',
                'data-message' => 'aucune région trouvée'
            ],
            'multiple' => true];
    }

    function filtre_geo_pays()
    {
        $options_pays = [];
        $code = $this->requete->ouArgs('pays', $this->valeur_par_defaut);
        if ($code) {
            $code = is_array($code) ? $code : [$code];
            $tab_pays = $this->sac->tab('pays');
            $result = array_intersect_key($tab_pays, array_flip($code));
            $options_pays = $this->filtre_options_select($result, $code, true);
        }

        return ['nom' => 'pays',
            'class' => 'autocomplete_select2 select2_autocomplete_pays',
            'options' => $options_pays,
            'attr' => [
                'data-url' => '/geo/pays?action=autocomplete',
                'data-message' => 'aucun pays trouvé'
            ],
            'multiple' => true];
    }

    function lister_zonage()
    {
        $tab_zonegroupe = table_simplifier($this->sac->tab('zonegroupe'));
        $tab = [];
        foreach ($tab_zonegroupe as $k => $titre) {
            $tab['zonegroupe_' . $k] = ['ajouter_zonage', [$k]];
        }
        return $tab;
    }

    function filtre_ajouter_zonage($args)
    {
        $id_zonegroupe = $args[0];
        $zonegroupe = $this->sac->tab('zonegroupe.' . $id_zonegroupe);
        if ($zonegroupe) {
            $tab_zone = $this->sac->tab('zone');
            $k = $zonegroupe['id_zonegroupe'];
            $titre = $zonegroupe['nom'];
            $tz = table_simplifier(table_filtrer_valeur($tab_zone, 'id_zonegroupe', $k));
            $tab_valeur = [];
            foreach ($tz as $j => $z) {
                $tab_valeur[$j] = ['valeur' => $j, 'libelle' => $z];
            }
            return ['nom' => $titre, 'class' => 'list-inline select2', 'options' => $tab_valeur, 'multiple' => true];
        }
    }

    function filtre_en_vrac($tab_nom_filtres)
    {
        $tab = [];
        foreach ($tab_nom_filtres as $nom_filtre) {
            $tab[$nom_filtre] = $this->filtre_simple($nom_filtre);
        }
        return $tab;
    }

    function filtre_simple($nom, $class = '')
    {
        return [
            'nom' => $nom,
            'class' => $class
        ];
    }

    function filtre_ajouter_inverse()
    {
        $checked = !($this->requete->ouArgs('inverse') === null);
        $options = [['on' => $checked, 'valeur' => 'inverse', 'libelle' => 'inverse_selection']];
        return [
            'id' => 'inverse_selection',
            'nom' => 'inverse_selection',
            'type' => 'checkbox',
            'options' => $options];
    }

    function liste_motgroupe($objet = null, $nom_filtre = 'mots')
    {

        $tab_motgroupe = $this->sac->tab('motgroupe');
        if ($objet) {
            $tab_motgroupe = table_filtrer_valeur_existe($tab_motgroupe, 'objets_en_lien', $objet);
        }
        $tab_motgroupe = table_filtrer_valeur($tab_motgroupe, 'systeme', false);
        $tab_groupe = [];
        foreach ($tab_motgroupe as $id => $groupe) {
            if (!empty($groupe['options'][$objet]['nom'])) {
                $id_g = str_replace([' ', '\''], '', $groupe['options'][$objet]['nom']);
                $nom = $groupe['options'][$objet]['nom'];
            } else {
                $id_g = $id;
                $nom = $groupe['nom'];
            }
            $operateur = 'AND';
            if (isset($groupe['options'][$objet]['operateur'])) {
                $operateur = $groupe['options'][$objet]['operateur'];
            }
            if (!isset($tab_groupe[$nom_filtre . $id_g])) {
                $tab_groupe[$nom_filtre . $id_g] = [
                    'operateur' => $operateur,
                    'nom' => $nom
                ];
            }
            $tab_groupe[$nom_filtre . $id_g]['id_motgroupe'][] = $id;
        }
        return $tab_groupe;
    }

    function lister_mots($objet = null, $prefixe = '', $exclure_groupe_mot = [])
    {

        $filtres = [];
        $tab_mot_ind = $this->sac->tab('mot');
        $tab_motgroupe = $this->sac->tab('motgroupe');
        $tab_motgroupe = table_filtrer_valeur($tab_motgroupe, 'systeme', false);
        if ($objet) {
            $tab_motgroupe = table_filtrer_valeur_existe($tab_motgroupe, 'objets_en_lien', $objet);
        }
        foreach ($exclure_groupe_mot as $id) {
            if (isset($tab_motgroupe[$id])) {
                unset($tab_motgroupe[$id]);
            }
        }
        if (!empty($tab_mot_ind)) {
            foreach ($tab_mot_ind as $k => $el) {
                $tab_mots[$k] = ['libelle' => $el['nom'], 'valeur' => $k];
            }
            $options_defaut = ['nom' => 'mots', 'opt_group' => true, 'classement' => '', 'indice' => ''];
            $tab_m = [];
            foreach ($tab_motgroupe as $id_groupe => $groupe) {
                $nom_filtre = $prefixe . 'mots' . $id_groupe;
                $filtres[$nom_filtre] = ['ajouter_mots', [$id_groupe, $objet, $prefixe]];
            }
        }
        return $filtres;

    }

    function filtre_ajouter_mots($args)
    {
        [$id_groupe, $objet, $prefixe] = $args;
        $mots = $this->requete->ouArgs($prefixe . 'mots', $this->valeur_par_defaut);
        $filtres = [];
        $tab_mot_ind = $this->sac->tab('mot');
        $groupe = $this->sac->tab('motgroupe.' . $id_groupe);

        $tab_mots = [];
        if (!empty($tab_mot_ind)) {
            $mots = (is_array($mots)) ? $mots : [$mots];
            foreach ($tab_mot_ind as $k => $el) {
                $tab_mots[$k] = ['libelle' => $el['nom'], 'valeur' => $k];
                if (in_array($k, $mots)) {
                    $tab_mots[$k]['on'] = true;
                }
            }
            $options_defaut = ['nom' => 'mots', 'opt_group' => true, 'classement' => '', 'indice' => ''];
            $tab_m = [];
            $options = $options_defaut;
            if (isset($groupe['options'][$objet])) {
                $options = array_merge($options, $groupe['options'][$objet]);
            }
            $nom_filtre = $prefixe . 'mots' . $id_groupe;

            if (!isset($tab_m[$nom_filtre])) {
                $tab_m[$nom_filtre] = [];
            }
            $tab_mot = array_intersect_key($tab_mots, table_filtrer_valeur($tab_mot_ind, 'id_motgroupe', $id_groupe));
            $nb_mot = count($tab_mot);
            $tab_m[$nom_filtre] = array_merge($tab_m[$nom_filtre], $tab_mot);
            if ($nb_mot > 0) {
                return [
                    'nom' => $groupe['nom'],
                    'options' => $tab_m[$nom_filtre],
                    'multiple' => true,
                    'class' => 'select2 list-inline'
                ];
            }


        }

        return null;
    }

    function getFiltresSimples()
    {
        $rechercher = $this->requete->get('search') ?? $this->liste_selection_defaut['search']['value'] ?? '';
        $tab_filtres['premier']['search'] = $this->filtre_ajouter_recherche();
        return $tab_filtres;
    }

    function filtre_ajouter_recherche()
    {

        return [
            'id' => 'recherche',
            'nom' => 'search[value]',
            'placeholder' => 'rechercher',
            'label' => '<i class="fa fa-search"></i>',
            'type' => 'text',
            'class' => 'champs_recherche',
            'class_conteneur' => 'label_icone',
            'value' => ($this->requete->ouArgs('search[value]', $this->valeur_par_defaut)) ?? ''];
    }

    function filtre_ajouter_choix($objet)
    {
        $nom = $objet;
        $tab = table_simplifier($this->sac->tab($objet));
        $value = array_keys($tab);
        $options = $this->filtre_options_select($tab, $value);
        $tab_filtres = ['nom' => $nom, 'class' => 'select2', 'options' => $options, 'multiple' => true];
        return $tab_filtres;
    }


    function filtre_limitation_id()
    {
        $valeur = $this->requete->ouArgs('limitation_id');
        return [
            'id' => 'limitation_id',
            'nom' => 'limitation_id',
            'placeholder' => 'limitation_id',
            'type' => 'text',
            'class' => '',
            'value' => $valeur];
    }


    function filtre_checkbox($args)
    {
        $nom_valeur = $args[0] ?? '';
        $libelle = $args[1] ?? '';
        $checked = !($this->requete->get($nom_valeur) === null);
        $libelle = empty($libelle) ? $nom_valeur : $libelle;
        $options = [['on' => $checked, 'valeur' => $nom_valeur, 'libelle' => $libelle]];
        return [
            'id' => $nom_valeur,
            'nom' => $nom_valeur,
            'type' => 'checkbox',
            'options' => $options
        ];
    }

    function filtre_checkbox_indetermine($nom_valeur, $libelle)
    {

        $checked = !($this->requete->get($nom_valeur) === null);
        $libelle = empty($libelle) ? $nom_valeur : $libelle;
        $options = [['on' => $checked, 'valeur' => $nom_valeur, 'libelle' => $libelle]];
        return [
            'id' => $nom_valeur,
            'nom' => $nom_valeur,
            'type' => 'checkbox',
            'options' => $options,
            'class' => 'icheck_ind',
            'attr' => ['indeterminate' => "true"]
        ];
    }


    function filtre_prepare_donnee_hierarchie($nom, $tab_valeur, $valeur)
    {
        $filtre = [
            'id' => $nom,
            'nom' => $nom,
            'type' => 'select2_hierarchie',
            'class' => 'select2_hierarchie'
        ];
        if ($tab_valeur) {
            foreach ($tab_valeur as $k => $val) {
                $temp = ['libelle' => $val['label'], 'valeur' => $k, 'class' => 'niveau' . $val['niveau'], 'on' => ($valeur == $val)];
                $filtre['options'][] = $temp;
            }
        }
        return $filtre;
    }


}
