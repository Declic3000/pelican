<?php

namespace Declic3000\Pelican\Component\Form;



use Declic3000\Pelican\Service\Requete;
use Declic3000\Pelican\Service\Sac;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormRegistryInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class FormFactory extends \Symfony\Component\Form\FormFactory
{

    protected $requete;
    protected $router;
    protected $sac;

    public function __construct(FormRegistryInterface $formRegistry, Requete $requete, Sac $sac, UrlGeneratorInterface $router)
    {
        parent::__construct($formRegistry);
        $this->requete = $requete;
        $this->sac = $sac;
        $this->router = $router;
    }

    function pregenerateForm($name, $type = \Symfony\Component\Form\Extension\Core\Type\FormType::class, $data = null, array $options = [], $modif = true, $args = [])
    {
        $page = $this->sac->get('page');
        $objet = $this->sac->get('objet');
        $cle = $this->sac->descr($objet . '.cle');
        if ($modif && $objet != '' && !isset($args[$cle])) {
            $args[$cle] = $this->sac->get('id');
        }
        $args['redirect'] ??= $this->requete->get('redirect');
        $options['required'] = false;
        $url = $this->sac->get('url_courante');
        $options['action'] = $url .(strpos((string) $url,'?')?"&":'?').http_build_query($args);
        return $this->createNamedBuilder($name, $type, $data, $options);
    }

    /**
     * {@inheritdoc}
     */
    public function generateForm($name, $type = \Symfony\Component\Form\Extension\Core\Type\FormType::class, $data = null, array $options = [], $modif = true, $args = [])
    {
        $formbuilder = $this->pregenerateForm($name, $type, $data, $options, $modif, $args);
        $formbuilder->add('submit', SubmitType::class, ['label' => 'Enregistrer', 'attr' => ['class' => 'btn-primary']]);
        return $formbuilder->getForm();
    }


}
