<?php

namespace Declic3000\Pelican\EntityExtension;

use Declic3000\Pelican\Service\Sac;
use Doctrine\ORM\EntityManager;

class EntityExtension
{

    protected $sac;
    protected $db;
    protected $em;

    function __construct(Sac $sac, EntityManager $em)
    {
        $this->sac = $sac;
        $this->db = $em->getConnection();
        $this->em = $em;
    }


}

